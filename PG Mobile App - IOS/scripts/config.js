(function (global) {
    var app = global.app = global.app || {};

    //app.gateway = "https://mobileapi-testing.pathway.com:9999/";
    //app.gateway = "https://mobileapi.pathway.com:9999/";
    app.gateway = "https://mobileapi.pathway.com/";
    app.gateway_version = 'v1';
    app.version = '1.3.9';
    app.mock = false;
    app.mock_not_found = false;
    app.mock_failed = false;
    app.emulatable_broken_connection = false;
	app.marketing_timeout = 86400000; //86400000 //300000
	app.messages = {
		shareHighRisk: 'We recommend caution when sharing your results. Pathway cannot be responsible for any consequences if you choose to share your test results, such as emotional issues, impact on life-changing decisions, potential genetic discrimination, and loss of confidentiality.',
		shareNotice: 'Sharing your test results can be fun and interesting. Pathway cannot be responsible for any consequences if you choose to share your test results, such as emotional issues, impact on life-changing decisions, potential genetic discrimination, and loss of confidentiality.',
		offlineSettings: 'Your device appears to be offline. Your preferences will be saved for this session only, unless your device is reconnected to the Internet.',
		offlineConsulting: 'Your device appears to be offline. Your request will be sent once your device is reconnected to the Internet.',
		emailChanges: 'An email notification has been sent to you regarding these changes',
		offlineGeneral: 'There was a problem contacting the server. Please try again later.',
		offlineExplict: 'It looks like your device is offline, please try again later.',
		reportFound: 'Thank you!<br><br>Your report has been located and connected to your App.',
		failedFollowup: 'I’m sorry, your support request could not be sent at this time.<br /><br />Please try again later.',
		offlineReporting: 'It looks like your device is offline. Please try connecting your report again when you have a network connection.'
    };

    app.routes = {
        "views/survey.html": {
            "ios": "views/ios/survey.html",
            "android": "views/android/survey.html"
        },
        "views/result.html": {
            "ios": "views/ios/result.html",
            "android": "views/android/result.html"
        },
        "views/reports.html": {
            "ios": "views/ios/reports.html",
            "android": "views/android/reports.html"
        },
        "views/reports-results-list.html": {
            "ios": "views/ios/reports-results-list.html",
            "android": "views/android/reports-results-list.html"
        },
        "views/categories.html": {
            "ios": "views/ios/categories.html",
            "android": "views/android/categories.html"
        },
        "views/categories-results-list.html": {
            "ios": "views/ios/categories-results-list.html",
            "android": "views/android/categories-results-list.html"
        },
        "views/associate-report.html": {
            "ios": "views/ios/associate-report.html",
            "android": "views/android/associate-report.html"
        }
    };
    
    if(app.gateway.indexOf('test') !== -1){
        setTimeout(function(){
            //alert('This is a test api gateway: ' + app.gateway);
        }, 5000);
    }
})(window);