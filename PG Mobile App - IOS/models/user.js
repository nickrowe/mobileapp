(function (root) {
    function User() {
    }
    User.prototype.get = function (variable) {
        return this[variable];
    };
    User.prototype.set = function (variable, value) {
        this[variable] = value;
        saveUser(this);
    };
    User.prototype.setMany = function (obj) {
        $.extend(this, obj);
        saveUser(this);
    };
    User.prototype.getPreference = function (identifier) {
        return this.get('preferences')['by_identifier'][identifier];
    };
    User.prototype.setPreference = function (identifier, value) {
        this.preferences.by_identifier[identifier] = value;
		this.preferences.preference_graph.forEach(function (group, i) {
			group.preferences.forEach(function (pref, j) {
				if (pref.identifier == identifier) {
					pref.value = value;
					if (app.accountPreferencesService.viewModel.preferences.preference_graph[i] != null) {
						app.accountPreferencesService.viewModel.preferences.preference_graph[i].preferences[j].value = value;
                    }
                }
            });
        });
        saveUser(this);
    };
    
    function saveUser(user) {
        //-- Get the user data
        var users = root.getData('users');
        if (users == null) {
            users = {};
        }
        
        //-- Save back to storage
        users[user.email] = JSON.stringify(user);
        
        root.setData('users', users);
    }
    
    function loadUser(email) {
        var users = root.getData('users');
        
        if (users == null) {
            users = {};
        }
        
        if (users[email] == null) {
            users[email] = {
                email: email
            };
        }
        
        if (typeof users[email] === 'string') {
            users[email] = JSON.parse(users[email]);
        }
		
		if (typeof users[email].preferences != 'object') {
			users[email].preferences = {
				preference_graph: [],
				all_preferences: {},
				by_identifier: {}
            };
        }
		
        return $.extend(users[email], new User());
    }
    
    app.getUser = loadUser;
}(app));