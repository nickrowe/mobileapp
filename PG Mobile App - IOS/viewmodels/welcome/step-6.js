(function (global) {
    var Model,
    	service,
        app = global.app = global.app || {};

    Model = function () {
        return kendo.observable({});
    };

    service = {
        waiting: true,
		beforeShow: function(e) {
		},
		init: function(e) {
            //FastClick.attach(e.view.content[0]);
		},
        show: function (e) {
    		app.application.hideLoading();
            var viewModel = service.viewModel;
        },
        afterShow: function (e) {
            app.layout.fillCenter(e.view.element);
			app.resizeView('step-6', service, e.view.element);
        },
        viewModel: new Model()
    };
    app.welcomeStep6Service = service;
})(window);