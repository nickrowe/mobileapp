(function (global) {
    var Model,
    	service,
        app = global.app = global.app || {};

    Model = function () {
        return kendo.observable({
            message: ''
        });
    };

    service = {
        waiting: true,
		beforeShow: function(e) {
		},
		init: function(e) {
            //FastClick.attach(e.view.content[0]);
		},
        show: function (e) {
    		app.application.hideLoading();
            var viewModel = service.viewModel;
            viewModel.set('message', e.view.params.message);
        },
        afterShow: function (e) {
            app.layout.fillCenter(e.view.element);
			app.resizeView('step-7', service, e.view.element);
        },
        viewModel: new Model()
    };
    app.welcomeStep7Service = service;
})(window);