(function (global) {
    var app = global.app = global.app || {},
		Model,
		service;
	
	Model = function () {
        return kendo.observable({
			preferences: {
				preference_graph: []
            },
			preferences_clone: {
				preference_graph: []
            },
			savePreferences: function (e, silent, redirect) {
				if (silent == null) {
					silent = false;
                }
				if (redirect == null) {
					redirect = true;
                }
				
				if (service.queue.length === 0) {
					// Do nothing there is nothing to save...
					if (!silent) {
						if (redirect) {
							app.application.navigate('#lifestyle-view');
                        }
                    }
					return;
                }
				
				var waitingList = [];
				var offline = false;
				var prompt = false;
				var queue = service.queue.slice();
				service.queue = [];
				queue.forEach(function (que, i) {
					var data = {
						value: que.value ? true : false
                    };
					var addToQueue = function () {
						app.queue.addItemFromParams(
							/* type */    app.queue.consts.savePrefs, 
							/* url */     'user/preferences/' + encodeURIComponent(que.identifier),
							/* data */    JSON.stringify(data),                                               
							/* options */ {
								type: 'POST',
								contentType: 'application/json'
                            }                          
						);
	                };
					
					if (navigator.network.connection.type === Connection.NONE) {
						// Clearly offline put into queue
						addToQueue();
						offline = true;
					} else {
						if (!silent) {
							app.application.showLoading();
						}
						if (que.identifier.indexOf('com.pathway.marketing.social.warnings') > -1) {
							prompt = true;
                        }
						waitingList.push(app.api.setPreference(que.identifier, que.value).then(function (data, textStatus, jqXHR) {
							console.log('settings - good', data, textStatus, jqXHR);
							if (data.status) {
								//app.api.getPreferencesCache().then(function (data) {
								//	//-- TODO get user settings and apply to view model...
									
	                            //});
				            } else {
								addToQueue();
				            }
				        }).fail(function (jqXHR, textStatus, errorThrown) {
							console.log('settings - fails', jqXHR, textStatus, errorThrown);
							addToQueue();
				        }));
	                }
					
                });
				
				if (!silent) {
					if (offline) {
						app.showNotice(app.messages.offlineSettings).then(function (e) {
							//...
							if (redirect) {
								app.application.navigate('#lifestyle-view');
                            }
		                });
	                } else {
						$.when.apply($, waitingList).always(function () {
							app.application.hideLoading();
							
							//-- All changes done
							if (prompt) {
								app.showNotice(app.messages.emailChanges).then(function (e) {
									if (redirect) {
										app.application.navigate('#lifestyle-view');
		                            }
				                });
                            } else {
								if (redirect) {
									app.application.navigate('#lifestyle-view');
	                            }
                            }
	                    });
	                }
                }
            }
    	});
    };

    service = {
		queue: [],
		pushToQueue: function (pref) {
			var pushToQue = true;
			var removeFromQue = false;
			var variable = 0;
			var removeFromQueIndex = variable;
			service.queue.forEach(function (que, k) {
				if (que.identifier === pref.identifier) {
					pushToQue = false;
					que.value = pref.value;
					removeFromQueIndex = k;
					for (var l in service.viewModel.preferences.preference_graph) {
						var pref_group = service.viewModel.preferences.preference_graph[l];
						for (var j in pref_group.preferences) {
							var pref2 = pref_group.preferences[j];
							if (pref.value == pref2.value) {
								removeFromQue = true;
                            }
                        }
                    }
                }
            });
			if (pushToQue) {
				service.queue.push({
					identifier: pref.identifier,
					value: pref.value
                });
            }
			else if (removeFromQue) {
				service.queue.splice(removeFromQueIndex, 1);
            }
        },
		changeMarketing: function (e) {
			var pref = {
				identifier: 'com.pathway.marketing.show-promotional-information',
				value: $(this).is(':checked') ? false: true
            };
			service.pushToQueue(pref);
			var user = app.getUser(app.getData('email'));
			user.setPreference(pref.identifier, pref.value);
			service.viewModel.savePreferences(e, true /* slient */);
			//-- Reset Timer
			user.set('marketing_slide_last_show', new Date().getTime());
        },
		changeRisk: function (e) {
			var riskLevel = $('#warning-modal').data('category');
			var pref = {
				identifier: null,
				value: $(this).is(':checked') ? false: true
            };
			if (riskLevel === 'low-risk') {
				pref.identifier = 'com.pathway.marketing.social.warnings.low-risk-phenotype';
            }
			else if (riskLevel === 'high-risk') {
				pref.identifier = 'com.pathway.marketing.social.warnings.high-risk-phenotype';
            }
			
			if (pref.identifier !== null) {
				service.pushToQueue(pref);
				var user = app.getUser(app.getData('email'));
				user.setPreference(pref.identifier, pref.value);
				service.viewModel.savePreferences(e, true /* slient */);
            }
        },
        init: function (e) {
			service.viewModel.bind('change', function (e) {
				if (e.field === 'preferences.preference_graph') {
					var user = app.getUser(app.getData('email'));
					service.viewModel.get('preferences.preference_graph').forEach(function (group, i) {
						group.preferences.forEach(function (pref, j) {
							var old_value = user.getPreference(pref.identifier);
							if (old_value != pref.value) {
								user.setPreference(pref.identifier, pref.value);
								service.pushToQueue(pref);
                            }
			            });
			        });
                }
            });
        },
		beforeShow: function (e) {
			if (!app.isLoggedIn()) {
				e.preventDefault();
				app.application.navigate('views/account/login.html');
			}
			var user = app.getUser(app.getData('email'));
			service.viewModel.set('preferences.preference_graph', user.preferences.preference_graph);
		},
        show: function (e) {
			app.layout.createMenu(e.view.header);
			service.queue = [];
        },
		afterShow: function (e) {
			
        },
		hide: function (e) {
			service.viewModel.savePreferences(e, false, false);
        },
		viewModel: new Model()
    };
	
	app.accountPreferencesService = service;
})(window);