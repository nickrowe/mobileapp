(function (global) {
    var Model,
    	service,
        app = global.app = global.app || {};

    Model = function () {
        return kendo.observable({
			height: 0,
            accession_no: '',
            date_of_birth: '',
            postal: '',
            name: '',
            nextStep: function () {
                $('#welcome-step-3-view input').blur();
				
                if (navigator.network.connection.type == Connection.NONE) {
					app.showError(app.messages.offlineReporting).then(function (e) {
						app.application.navigate('#lifestyle-view');
                    });
					return;
                }
				
                //-- Start For Andriod
                service.viewModel.set('accession_no', $('#welcome-step-3-view [name=accession_no]').val());
				//-- End For Android
                
                var data = service.viewModel.toJSON();
                if (typeof data.date_of_birth === 'string') {
                    if (data.date_of_birth.length == 0) {
                        data.date_of_birth = $('#welcome-step-3-view .step-3-dob').val();
                    }
                }
                data.date_of_birth = (new moment(data.date_of_birth)).format('YYYY-MM-DD');
                
                service.viewModel.set('date_of_birth', data.date_of_birth);
                
                var user = app.getUser(app.getData('email'));
                user.set('date_of_birth', data.date_of_birth);
                
				app.application.navigate('views/welcome/step-4.html?' + [
                    "name=" + encodeURIComponent(service.viewModel.get('name')),
                    "date_of_birth=" + encodeURIComponent(service.viewModel.get('date_of_birth')),
                    "postal=" + encodeURIComponent(service.viewModel.get('postal')),
                    "accession_no=" + encodeURIComponent(service.viewModel.get('accession_no'))
                ].join('&'));
            }
        });
    };

    service = {
        waiting: true,
		beforeShow: function(e) {
		},
		init: function(e) {
			var content = e.view.content;
            
            function makeUppercase(input){
                //always make it capitalized on Android because of keyboard problems.
                if(device.platform === 'Android' || input.val().length){
                	input.css('text-transform', 'uppercase');
                }else{
                    input.css('text-transform', 'none');
                }
            }
            
            //auto capitalize
            content.find('.capitalize').on('keyup', function(){
                makeUppercase($(this));
            });
            content.find('.capitalize').each(function(){
                makeUppercase($(this));
            });
            //FastClick.attach(e.view.content[0]);
		},
        show: function (e) {
    		app.application.hideLoading();
            var viewModel = service.viewModel;
            
            //-- Auto fill the birthday with what is stored on the device
            var user = app.getUser(app.getData('email'));
            viewModel.set('date_of_birth', user.get('date_of_birth'));
            viewModel.set('accession_no', e.view.params.accession_no);
            
            if (viewModel.get('name').length === 0) {
                viewModel.set('name', user.get('last_name') + ', ' + user.get('first_name'));
            }
            
            /*
            accession_no: '',
            date_of_birth: '',
            postal: '',
            name: '',
            */
        },
        afterShow: function (e) {
            app.layout.fillCenter(e.view.element);
			app.resizeView('step-3', service, e.view.element);
        },
        viewModel: new Model()
    };
    app.welcomeStep3Service = service;
})(window);