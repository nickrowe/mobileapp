(function (global) {
	var ENCRYPTED = 1,
	    UNENCRYPTED = 2,
	    TRYCACHEBEFORE = 3,
	    TRYCACHEAFTER = 4,
	    NOCACHE = 5;
	/**
	 * @constructor
	 * @param {object} options
	 */
	var request = function (reqOptions) {
		var defaultOptions = {
			dataType: 'JSON',
			maxTimeout: 10000, // 10 seconds
			retryRate: 1000, // 1 second
			retryMax: 3,
			cacheType: NOCACHE,
			cacheTry: TRYCACHEAFTER,
			cacheExpire: 18000000, // 5 Hours
			reauth: true,
			default: false,
			mock: false
        };
		
		var options = $.extend({}, defaultOptions, reqOptions);
		
		options.retryAttempts = options.retryAttempts || 0;
		options.deferred = options.deferred || new $.Deferred();
		
        //-- If mocked replace the gateway for localfiles
        var api_call_url = app.gateway + app.gateway_version + '/' + options.url;
        if (options.mock && app.mock) {
            api_call_url = 'mocks/' + options.url;
        }
		
		var retry = function (options) {
			if (options.retryAttempts < options.retryMax - 1) {
				options.retryAttempts += 1;
				setTimeout(function () {
					new request(options).always(function () {
						//console.log('retried');
                    });
                }, options.retryRate);
				return false;
            } else {
				return true;
            }
        };
		
		var reAuth = function (options) {
			var deferred = new $.Deferred();
			
            var password;
            var email = app.getData('email');
            app.getData('password', function (password) {
				app.api.login(email, password)
				.done(function () {
					deferred.resolve();
                })
				.fail(function () {
					deferred.reject();
                });
			});
			
			return deferred.promise();
        };
		
		var continueable = false;
		
		if (options.cacheType !== NOCACHE && options.cacheTry === TRYCACHEBEFORE) {
			if (options.cacheType === ENCRYPTED) {
				var cache = window.localStorage.getItem('cache.' + options.url);
                if (cache) {
                    try {
                        isCache = true;
						var cached = JSON.parse(cache);
						var now = new Date().getTime();
						if (cached.time && now - cached.time < options.cacheExpire) {
							// TODO: finish caching
                        }
                    } catch (e) {
						// TODO: if cache fails continue
                    }
                }
            }
        }
		
		
        //-- If they are offline
        if (navigator.network.connection.type === Connection.NONE) {
			continueable = retry(options);
		} else {
			continueable = true;
        }
		
		if (continueable) {
			var urlOptions = $.extend({}, options, {url: api_call_url, timeout: options.maxTimeout});
			$.ajax(urlOptions)
			.done(function (data, textStatus, jqXHR) {
				if(request.status == 401) {
                    reAuth(options)
					.done(function () {
                        new request(options);
                    }).fail(function () {
						options.deferred.reject(data, textStatus, jqXHR);
                    });
				}
				
				if (options.cacheType !== NOCACHE) {
					var stringData;
					if (typeof data === object) {
						stringData = JSON.stringify(data);
                    } else {
						stringData = data;
                    }
					
					var cacheObject = {
						time: (new Date()).getTime(),
						data: stringData
                    };
					
					var cacheKey = 'cache.' + options.url;
					var cacheString = JSON.stringify(cacheObject);
					
					if (options.cacheType === UNENCRYPTED) {
						window.localStorage.setItem(cacheKey, cacheString);
                    } else if (options.cacheType === ENCRYPTED) {
						app.setDate(cacheKey, cacheString);
                    } else {
						throw "Unsupported cacheType option";
                    }
                }
				
				options.deferred.resolve(data, textStatus, jqXHR);
	        })
			.fail(function (jqXHR, textStatus, errorThrown) {
				if(jqXHR.status == 401) {
                    reAuth(options)
					.done(function () {
                        new request(options);
                    }).fail(function () {
						
                    });
					return;
				} else if(request.status == 400) {
                    options.deferred.reject(jqXHR, textStatus, errorThrown);
				} else {
					var failedRetry = retry(options);
					
					if (failedRetry) {
						options.deferred.reject(jqXHR, textStatus, errorThrown);
	                } else {
						
                        if (navigator.network.connection.type == Connection.NONE) {
                			var isCache = false;
                            //-- Check to see if there is a cache first version
                            if (options.cacheType !== NOCACHE && options.cacheTry === TRYCACHEAFTER) {
                                var cache = window.localStorage.getItem('cache.' + options.url);
                                if (cache) {
                                    try {
                                        isCache = true;
										var cached = JSON.parse(cache);
										//-- TODO: check expire!
										options.deferred.resolve(cached.data, textStatus, jqXHR);
                                    } catch (e) {
										options.deferred.reject(e, textStatus, errorThrown);
                                    }
                                    return;
                                }
                            }
                            //-- Else check to see if there is a default version
                            if (options.default && !isCache) {
                                //-- just try again to see if it is still offline
                                app.api.call(options, true);
                                return;
                            }
                        }
                    }
                }
	        });
        }

		return options.deferred.promise();
    }
	
	window.app.Request = request;
}(window));