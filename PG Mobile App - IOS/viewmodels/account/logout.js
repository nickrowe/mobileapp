(function (global) {
    var app = global.app = global.app || {};
	
	app.logoutService = {
		beforeShow: function (e) {
			app.application.showLoading();
        },
		afterShow: function (e) {
            app.loginService.logout().done(function () {
                app.application.navigate('views/introduction.html');
            });
        }
    };
}(window));