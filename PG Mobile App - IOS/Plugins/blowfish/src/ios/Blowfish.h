//
//  Blowfish.h
//  Pathway Genomics
//
//  Created by minidev on 6/25/14.
//
//

#ifndef Pathway_Genomics_Blowfish_h
#define Pathway_Genomics_Blowfish_h

#import <Foundation/Foundation.h>

#import "blowfish_lib.h"

#import <Cordova/CDVPlugin.h>
#import <Cordova/CDVJSON.h>

#import "AppDelegate.h"


const char *key = "chJfvVr7LVWzLyK3PfcJhxRY4YVeFoYPXsGhK0JLIqtc8CR2ZgjNfWhRPB54had9";

@interface Blowfish : CDVPlugin

// Open / Close
-(void) encode: (CDVInvokedUrlCommand*)command;
-(void) decode: (CDVInvokedUrlCommand*)command;

@end

#endif
