(function (global) {
    var Model,
    	service,
        app = global.app = global.app || {};

    Model = function () {
        return kendo.observable({});
    };

    service = {
        init: function (e) {
            app.externalLinks(e);
        },
        show: function (e) {
			app.layout.createMenu(e.view.header);
        },
        viewModel: new Model()
    };
    app.privacyAndTermsService = service;
})(window);