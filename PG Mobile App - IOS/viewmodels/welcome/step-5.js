(function (global) {
    var Model,
    	service,
        app = global.app = global.app || {};

    Model = function () {
        return kendo.observable({
			height: 0,
            phone_number: '',
            nextStep: function() {
                $('#welcome-step-5-view input').blur();
				
                if (navigator.network.connection.type == Connection.NONE) {
					app.application.navigate('#lifestyle-view');
                	app.showError(app.messages.offlineReporting);
					return;
                }
				
				app.application.showLoading();
                var phone_number = service.viewModel.get('phone_number');
                var email = service.viewModel.get('email');
				var failedMsg = app.messages.failedFollowup;
                app.api.sendFollowupRequest(phone_number, email).done(function (resp) {
                    if (resp.status) {
                        app.api.sync().then(function () {
    						app.application.hideLoading();
    					    app.application.navigate('views/welcome/step-7.html?message=' + encodeURIComponent(resp.message));
                        });
                    } else {
						var message = resp.message || failedMsg;
					    app.application.navigate('views/welcome/step-7.html?message=' + encodeURIComponent(message));
                    }
                }).fail(function (resp) {
	                if (navigator.network.connection.type == Connection.NONE) {
						app.showError(app.messages.offlineReporting).then(function (e) {
							app.application.navigate('views/categories.html');
	                    });
						return;
	                }
					
					var message = resp.message || failedMsg;
					app.application.navigate('views/welcome/step-7.html?message=' + encodeURIComponent(message));
                });
                
            }
        });
    };

    service = {
        waiting: true,
		beforeShow: function(e) {
		},
		init: function(e) {
            //FastClick.attach(e.view.content[0]);
		},
        show: function (e) {
    		app.application.hideLoading();
            var viewModel = service.viewModel;
            
            var user = app.getUser(app.getData('email'));
            viewModel.set('email', user.get('email'));
            app.api.getFollowupDefaults().then(function (resp) {
                viewModel.set('phone_number', resp.phone_number);
            });
        },
        afterShow: function (e) {
            app.layout.fillCenter(e.view.element);
			app.resizeView('step-5', service, e.view.element);
        },
        viewModel: new Model()
    };
    app.welcomeStep5Service = service;
})(window);