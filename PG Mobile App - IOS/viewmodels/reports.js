(function (global) {
    var app = global.app = global.app || {},
    	view;
    
    function updateNoResultsDiv(){
        if(app.pages.data.reports.length){
			view.content.find('.no-results').hide();
		}else{
			view.content.find('.no-results').show();
		}
    }
	
	var Model = function () {
        return kendo.observable({
            //reports_preview_slides: [
			//	'<div style="padding: 1em"><img src="http://st.depositphotos.com/1001992/1987/i/950/depositphotos_19871785-Beautiful-woman-with-fresh-skin-of-face.jpg" style="margin-left: 0.25em; width: 50%; height: 50%; float: right;">' +
			//	'Among women, breast cancer is the second leading cause of cancer death. ' + 
			//	'One in eight women will be affected by breast cancer in their lifetime' +
			//	'</div><p>' +
			//	'Pathway Genomics\' BRCATrue is a next-generation' + 
			//	'<a href="http://pathway.com/product" target="_blank" class="btn good-action-button" style="position: absolute; bottom: 0; right: 1em; margin: 0">Learn More</a>',
			
			 
			//	'bar'
			//],
            reports_preview_slides_default: ['abc'],
            reports_preview_slides: ['abc'],
			reports_icons: [],
			scrolling: function (e) {
				var scrollview = $('#reports-preview-scrollview > div');
				var old = scrollview.css('-webkit-transform');
				var x = old.match(/\d+/g)[1];
				//var y = old.match(/\d+/g)[2];
				//var z = old.match(/\d+/g)[3];
				scrollview.css({
					'-webkit-transform': 'translate3d(' + x + 'px, 0px, 0px)'
				});
				if ($('#reports-preview-scrollview').getKendoMobileScrollView()) {
					$('#reports-preview-scrollview').getKendoMobileScrollView().pane.dimensions.y.enabled = false;
                }
            },
    	});
    };

    var service = {
        init: function (e) {
            view = e.view;
            
            $(document).on('app_data_set', function(){
                view.scroller.reset();
                updateNoResultsDiv();
            });
			
			var mapReportsToIcons = function () {
				var applicable_ids = app.data.applicable.reports.ids.map(function (e) { return parseInt(e); });
				var marketing_ids = app.data.marketing.reports;
				var unapplicaple_reports = [],
					applicaple_reports = [];
				for (var key in app.data.reports) {
					var report = app.data.reports[key];
					var report_id = parseInt(app.data.reports[key].id);
					var marketing_index = marketing_ids.indexOf(report_id);
					if (marketing_index > -1) {
						if (report.display && report.display.colors.inactive) {
							if (report.display.colors.inactive.indexOf('#') > -1) {
								report.display.colors.inactive = app.hextoRGBA(report.display.colors.inactive);
                            }
                        }
						report.is_applicable = false;
						unapplicaple_reports[marketing_index] = report;
						//unapplicaple_reports.push(report);
					}
					else if (applicable_ids.indexOf(report_id) > -1) {
						report.is_applicable = true;
						applicaple_reports.push(report);
                    }
				}
				var report_icons = applicaple_reports.concat(unapplicaple_reports);
				app.reports.viewModel.set('reports_icons', report_icons);
            };
			
			mapReportsToIcons();
			
			app.pages.bind('change', function (e) {
	
				if(e.field.indexOf('data') !== -1) {
					mapReportsToIcons();	
                }
            });
			
			var touchPoint = {
				x: 0,
				y: 0
            };
			
			var userEvents = new kendo.UserEvents($('#page-results .tiles-container'), {
                press: function(e) {
                    touchPoint = {
                        y: e.touch.y.client,
                        x: e.touch.x.client,
						time: new Date().getTime()
                    };
				},
                release: function(e) {
					var deltaPoint = {
                        y: e.touch.y.client,
                        x: e.touch.x.client,
						time: new Date().getTime()
                    };
					
                    if (Math.abs(touchPoint.y - deltaPoint.y) < 10 && Math.abs(touchPoint.x - deltaPoint.x) < 10) {
						e.event.preventDefault();
	                    var li = $(e.event.target).closest('li');
	                    var report_id = li.data('report-id');
	                    var applicable = li.data('applicable');
	                    var uid = li.data('uid');
	                    var title = li.data('title');
						document.activeElement && document.activeElement.blur();
						document.activeElement = null;

						if (report_id == null) {
							return;
                        }
						
						setTimeout(function () {
							if (applicable) {
								setTimeout(function () {
									app.application.navigate('#views/reports-results-list.html?report_id=' + report_id + '&title=' + title);
				                }, 20);
                            } else {
								var item = service.viewModel.reports_icons.find(function (e, i) {
									if (e.uid === uid) {
										return e;
                                    }
                                }, null);
								
								if (item != null) {
									app.reports.showReportsPreview.bind(item)(null, false);
                                }
                            }
                        }, 20);
                    }
                }
            });
        },
		showReportsPreviewReset: function (e) {
			service.viewModel.set('reports_preview_slides', service.viewModel.get('reports_preview_slides_default'));
			$('#reports-preview-scrollview').getKendoMobileScrollView().unbind('changing');
        },
		goToConsult: function (evt) {
			evt.preventDefault();
			$('#preview-closer').hide();
			app.application.navigate(this.element.attr('href'));
        },
		showReportsPreview: function (e, marketing) {
			if (marketing == null) {
				marketing = true;
            }
			app.modal('reports-preview', null, '');
			var that = this;
			var report_ids = app.data.marketing.reports;
			var report_index = 0;
			var resizeScroller = function (e) {
				var scroll_container = this.element.find('.km-scroll-container');
				var page = this.element.find('.km-virtual-page');
				scroll_container.height(page.height() - page.find('.title').height() - page.find('.action a').outerHeight() - 5);
            }
			
			//-- Reposition Popup
			$('.reports-preview-modal-wrapper-wrapper .k-animation-container').css({
				'max-height': '742px',
				'max-width': '490px'
            });
			
			
			if (service.viewModel.get('reports_preview_slides.length') === 1 && service.viewModel.get('reports_preview_slides[0]') === 'abc') {
				var report_pages = [];
				
				//-- Pull all reports into a scrollview
				report_ids.forEach(function (report_id, i) {
					for (var key in app.data.reports) {
						if (app.data.reports[key].id == report_id) {
							var item = app.data.reports[key];
							if (item.view != null) {
								var view = $(item.view);
								//var reqConsult = $('<a href="#views/consult/request.html?report_id=' + encodeURIComponent(report_id) + '" data-click="app.reports.goToConsult" data-role="button">Request Consultation</a>')
								//reqConsult.appendTo(view.find('.action'));
								var title = view.find('.title');
								var scrolling_box = view.find('.scrolling-box');
								title.attr(
									'style', 
									title.attr('style').replace('-color', '')
                                );
								var title_text = title.html();
								title.html('');
								$('<span class="align-center"></span>').css({
									'display': 'inline-block',
									'vertical-align': 'middle'
                                }).html(title_text).appendTo(title);
								
								var title_height = title.outerHeight();
								var scroll_height = $('.reports-preview-modal-wrapper-wrapper .k-animation-container').height() - title_height;
								
								
								scrolling_box
									//.css({'height': scroll_height})
									//.css({
									//	overflow: 'scroll'
	                                //})
									.attr('data-role', 'scroller')
									.attr('data-use-native-scrolling', 'true');
								report_pages.push(view.html());
	                        }
							break;
		                }
	            	}
	            });
				
				service.viewModel.set('reports_preview_slides', report_pages);
				
				$('#reports-preview-scrollview').getKendoMobileScrollView().bind('changing', resizeScroller);
            }
				
			//-- Find the index to go to
			report_ids.forEach(function (report_id, i) {
				if (report_id == that.id) {
					report_index = i;
                }
            });
			//-- Scroll to that index
			$('#reports-preview-scrollview').getKendoMobileScrollView().scrollTo(report_index);
			
			var modal = $('#reports-preview-modal');
			$('#report-preview-css').html(app.data.media.css['report-popup']);
			kendoFix.adjustScrollViewHeight(modal);
			
			var offset = modal.offset();
			if ($('#preview-closer').length === 0) {
				var close = $('<span id="preview-closer"><span class="close">&times;</span></span>');
				close.prependTo(document.body);
				var margin = parseInt($('#preview-closer .close').css('margin'));
				close.css({
					position: 'absolute',
					top: offset.top - (close.width() / 3),
					left: offset.left + modal.outerWidth() - ((close.width() / 3) * 2)
	            });
				close.bind('touchstart', function (evt) {
					evt.preventDefault();
					$('#reports-preview-modal').getKendoMobileModalView().close();
					$(this).hide();
                });
            } else {
				$('#preview-closer').show();
            }
				
			
			//-- Hide show me how on changes
			var user = app.getUser(app.getData('email'));
			var keepMeUpdated = user.getPreference('com.pathway.marketing.show-promotional-information');
			if (!marketing || (keepMeUpdated != null && keepMeUpdated === false)) {
				$('.ask-show-me').hide();
            } else {
				$('.ask-show-me').show();
				$('.ask-show-me input[type=checkbox]').attr('checked', false);
            }
			
			//-- Reduce margin if there are too many page indicators
			if (service.viewModel.get('reports_preview_slides.length') > 5) {
				$('#reports-preview-modal .show-pages .km-pages').addClass('lot-of-pages');
            } else {
				$('#reports-preview-modal .show-pages .km-pages').removeClass('lot-of-pages');
            }
			
			//-- Resize vertical scroller
			$('#reports-preview-scrollview').getKendoMobileScrollView().trigger('changing');
        },
		
		beforeShow: function(e) {
			if(!app.isLoggedIn()) {
				e.preventDefault();
				app.application.navigate('views/account/login.html');
			}
		},
        show: function(e){
			app.layout.createMenu(e.view.header);
            e.view.header.find('[data-role=view-title]').text(e.view.params.title);
            
			updateNoResultsDiv();
			
        },
		afterShow: function (e) {
			//e.view.header.find(".km-tabstrip").data("kendoMobileTabStrip").switchTo('#views/reports.html');
			$('[data-layout="layout-menu-tabs"] a[data-role="tab"]:nth-child(1)').removeClass('km-state-active');
			$('[data-layout="layout-menu-tabs"] a[data-role="tab"]:nth-child(2)').addClass('km-state-active');
        },
		viewModel: new Model()
    };
	
	app.reports = service;
})(window);