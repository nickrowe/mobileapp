(function (global) {
    var LoginViewModel,
        app = global.app = global.app || {};

    LoginViewModel = function () {
        return kendo.observable({
            //demo_email: 'example_user_demo@pathway.com',
            //demo_password: 'nothing!2',
            email: '',
            password: '',
            remember: false,
			
			goToRegister: function (e) {
				setTimeout(function () {
					app.application.navigate('#views/account/register.html');
                }, 50);
            },

            onLogin: function (e) {
                if (e != null) {
                    e.preventDefault();
                }
				$('input').blur();
				
                var that = this,
                    email = that.get("email"),
                    password = that.get("password"),
                    remember = that.get("remember");
                
                email = email != null ? email.trim() : '';
                password = password != null ? password.trim() : '';

                ////let them log into the demo user with any password
                //if(email === app.loginService.viewModel.demo_email){
                //    password = app.loginService.viewModel.demo_password;
                //}

                this.login(email, password, remember);
            },

            clearForm: function () {
                var that = this;

                that.set("email", "");
                that.set("password", "");
            },

            checkEnter: function (e) {
                var that = this;

                if (e.keyCode == 13) {
                    $(e.target).blur();
                    that.onLogin();
                }
            },

            login: function (email, password, remember, accession_no, date_of_birth, registered) {
                var that = this;
			    var deferred = $.Deferred();
				
    			//-- Loader
                //app.application.changeLoadingMessage('Attempting to log you in...');

				//first force log out
				app.loginService.logout(false).done(function(){
    				//app.application.showLoading();

					function complete(data, textStatus, request){
						var errors = [];
						app.log(data);
						if(typeof data.errors != 'undefined' && Object.keys(data.errors).length) {
							for(var field in data.errors) {
								errors.push(data.errors[field]);
							}
						}
						else if(data.status !== true || request.status != 200){
							//according to docs, request.status will be 403 on failure
							errors.push(app.messages.offlineGeneral);
						}

						if(errors.length) {
							app.application.hideLoading();
							app.showError(errors.join('<br />'));
						} else {
                            app.setData('email', email);
                            app.setData('password', password);
                            app.setData('remember', remember ? 1 : 0);
                            
                            var initial = '#lifestyle-view';
							if (registered) {
								initial = 'views/welcome/step-1.html';
                            }
                            
                            var process;
                            if (accession_no != null && date_of_birth != null && accession_no.length > 0) {
                                //-- Associate report if possible
                                process = app.api.addReport(accession_no, date_of_birth).then(function (resp) {
									if (!resp.status) {
                                		//initial = 'views/welcome/step-2.html?accession_no=' + encodeURIComponent(accession_no);
                                    } else {
										var message = app.messages.reportFound;
										initial = 'views/welcome/step-7.html?message=' + message;
                                    }
									return this.promise();
                                });
                            }
                            
                            if (registered) {
                                if (initial.indexOf('?') === -1) {
                                    initial += '?registered=1';
                                } else {
                                    initial += '&registered=1';
                                }
                            }
                            
                            $.when(process).always(function (resp) {
								
                                //-- Api returns DOB & Name of previously registered account now new one, so ignore.
                                var updateUser;
                                if (date_of_birth == null) {
                                    updateUser = app.api.getUserDetails();
                                }
								
								var optionalDeferred = $.Deferred();
								$.when(app.api.getReferralsCache(), app.api.getPreferencesCache()).always(function () {
									optionalDeferred.resolve();
			                    });
                                
                                return $.when(app.api.sync(), updateUser, optionalDeferred).then(function () {
                                    app.setData('used_before', 1);
                                    app.setData('last_activity_date', $.now());
                                    
    								app.application.hideLoading();
    								app.application.navigate(initial);
                                    deferred.resolve();
                                });
                            });
						}
					}
					
					app.api.call({
						request: 'auth/login',
						mockable: true,
						type: 'POST',
						dataType: 'JSON',
						data: {
							email: email,
							password: password
						},
						onComplete: function(data, textStatus, request){
							complete(data, textStatus, request);
						},
                        onError: function(data, textStatus, request){
    						var errors = [];
    						//app.log(data);
    						if(typeof data.errors != 'undefined' && Object.keys(data.errors).length){
    							for(var field in data.errors){
    								errors.push(data.errors[field]);
    							}
    						}
    						else if(data.status !== true || request.status != 200){
    							//according to docs, request.status will be 403 on failure
    							errors.push('There was an error logging in.');
    						}

                            if (navigator.network.connection.type == Connection.NONE) {
                            	app.showError('Looks like your device is offline. Please try again when you have a network connection.');
                            } else {
        						if(errors.length) {
        							app.application.hideLoading();
        							app.showError(errors.join('<br />'));
        						}
                            }
                            
                            deferred.reject();
                        }
					});
				});
                
                return deferred.promise();
            }
    	});
    };

    app.loginService = {
		beforeShow: function(e){
			//if already logged in, go to categories page
			if(app.isLoggedIn()){
				e.preventDefault();
				app.application.navigate('#lifestyle-view');
			}else{
				app.api.resetSyncData();
			}

            var email = app.getData('email');
            app.loginService.viewModel.set('email', email);
            app.loginService.viewModel.set('password', '');
            app.loginService.viewModel.set('remember', app.getData('remember'));

            //if(email && email !== app.loginService.viewModel.get('demo_email')){
            //    app.loginService.viewModel.set('email', email);
            //    app.loginService.viewModel.set('password', '');
            //}else{
            //    app.loginService.viewModel.set('email', app.loginService.viewModel.get('demo_email'));
            //    app.loginService.viewModel.set('password', app.loginService.viewModel.get('demo_password'));
            //}
		},
		init: function(e) {
			//e.view.content.find('#forgot-password-btn').kendoTouch({
			//	tap: function(e){
			//		e.event.preventDefault();		//does this event work?

			//		app.showNotice('<div style="text-align: center">Please contact us at <br><a href="tel:18775057374">+1 (877) 505-7374</a></div>', 'Contact Us');
			//	}
			//});
            FastClick.attach(e.view.content[0]);
		},
        auth: function (e) {
            /*if (app.isLoggedIn()) {
                app.application.navigate('views/events/month.html');
            }*/
        },
        show: function (e) {
			/*
            var service = app.loginService;
            var viewModel = service.viewModel;

            if (app.session.loggedIn) {
                viewModel.set('isLoggedIn', true);
            } else {
                viewModel.set('isLoggedIn', false);
            }
			*/
        },
		afterShow: function(e) {
            navigator.splashscreen.hide();
		},

        viewModel: new LoginViewModel(),

        clearData: function(){
            app.api.resetSyncData();
			app.layout.resetMenu();
			app.reports.showReportsPreviewReset();
			app.queue.clear();
			
			if ($('#categories_results_list_accordion').getKendoPanelBar()) {
				$('#categories_results_list_accordion').getKendoPanelBar().destroy();
            }
			
			app.categories.touch = null;
            
            var rememberThePromise = app.getData('remember');
            var reset = function (remember) {
		        if (!remember) {
					app.removeData('password');
		        }
				app.removeData('data');
				app.removeData('data_date');
				app.removeData('last_activity_date');
                app.removeData('force_no_reports');
            };
            
            if (typeof rememberThePromise === 'undefined') {
                reset(0);
            } else {
                reset(rememberThePromise);
            }
        },

		logout: function (hide_loading_when_done) {
            var hide_loading_when_done = typeof hide_loading_when_done === 'undefined' ? true : false;
			var deferred = $.Deferred();

			function complete(){
				app.loginService.clearData();
			}

			if(app.test_local){
				complete();
				deferred.resolve();
				return deferred.promise();
			}

            app.application.showLoading();
			app.api.call({
				request: 'auth/logout',
				mockable: true,
				type: 'POST',
				dataType: 'JSON',
				onComplete: function(data, status, request){
                    if(hide_loading_when_done){
                		app.application.hideLoading();
                    }
					/*if(request.status != 200){
						app.showError('There was an error logging out');
						return false;
					}*/
					app.removeData('remember');
					//-- Wipe clean the data from views
					var services = [
						 'welcomeStep1Service'
						,'welcomeStep2Service'
						,'welcomeStep3Service'
						,'welcomeStep4Service'
						,'welcomeStep5Service'
						,'welcomeStep6Service'
						,'welcomeStep7Service'
						//'associateReportService'
					];
					services.forEach(function (service) {
						var data = app[service].viewModel.toJSON();
						var key;
						for (key in data) {
							var value;
							if (typeof key === 'number') {
								value = 0;
                            }
							else if (typeof key === 'string') {
								value = '';
                            } else {
								value = null;
                            }
							app[service].viewModel.set(key, value);
                        }
                    });
					app.associateReportService.viewModel.set('fields', {});
					complete();
					deferred.resolve();
				},
                onError: function(){
                    if(hide_loading_when_done){
                		app.application.hideLoading();
                    }

                    complete();
					deferred.resolve();
                }
			});

			return deferred.promise();
		}
    };
})(window);