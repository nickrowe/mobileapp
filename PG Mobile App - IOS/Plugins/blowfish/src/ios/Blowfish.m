//
//  Blowfish.m
//  Pathway Genomics
//
//  Created by minidev on 6/25/14.
//
//

#import "Blowfish.h"
#include "blowfish.h"
#import "Base64.h"
#import <CommonCrypto/CommonDigest.h>

@implementation NSString (MD5)

- (NSString *)MD5String {
    const char *cstr = [self UTF8String];
    unsigned char result[16];
    CC_MD5(cstr, strlen(cstr), result);
    
    return [NSString stringWithFormat:
            @"%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X",
            result[0], result[1], result[2], result[3],
            result[4], result[5], result[6], result[7],
            result[8], result[9], result[10], result[11],
            result[12], result[13], result[14], result[15]
            ];  
}

@end

@implementation Blowfish

int64_t charTo64bitNum(char a[]) {
    int64_t n = 0;
    n = (((int64_t)a[0] << 56) & 0xFF00000000000000U)
    | (((int64_t)a[1] << 48) & 0x00FF000000000000U)
    | (((int64_t)a[2] << 40) & 0x0000FF0000000000U)
    | (((int64_t)a[3] << 32) & 0x000000FF00000000U)
    | ((a[4] << 24) & 0x00000000FF000000U)
    | ((a[5] << 16) & 0x0000000000FF0000U)
    | ((a[6] <<  8) & 0x000000000000FF00U)
    | (a[7]        & 0x00000000000000FFU);
    return n;
}

char* int64ToChar(char a[8], int64_t n) {
    memcpy(a, &n, 8);
    
    return (char*)a;
}

unsigned long long quickhash64(const char *str, unsigned long long mix)
{ // set 'mix' to some value other than zero if you want a tagged hash
    const unsigned long long mulp = 2654435789;
    
    mix ^= 104395301;
    
    while(*str)
        mix += (*str++ * mulp) ^ (mix >> 23);
    
    return mix ^ (mix << 37);
}

uint16_t* get_key() {
    NSString *uuid = [UIDevice currentDevice].identifierForVendor.UUIDString;
    NSString *md5 = [uuid MD5String];
//    NSLog(@"%@", [md5 substringToIndex:8]);
    const char *uuid_cstr = [[md5 substringToIndex:8] cStringUsingEncoding:NSASCIIStringEncoding];
//    unsigned long long hashed = quickhash64(uuid_cstr, (unsigned long long) 0);
//    const char hashed_cstr[8];
//    char *ckey = int64ToChar(hashed_cstr, hashed);
    uint16_t *key = (unsigned short *) uuid_cstr;
    
//    snprintf(key, 4, "%hu%hu%hu%hu",
//        ckey[0], ckey[1], ckey[2], ckey[3]
//    );
    
    return key;
}


void trythis() {
//    uint64_t uid = 14467240737094581;
//    
//    NSString *uuid = [UIDevice currentDevice].identifierForVendor.UUIDString;
//    const char *uuid_cstr = [uuid cStringUsingEncoding:NSASCIIStringEncoding];
//    unsigned long long hashed = quickhash64(uuid_cstr, (unsigned long long) 0);
//    const char *hashed_cstr;
//    int64ToChar(hashed_cstr, hashed);
    
//    NSString *baseCharacters = @"23456789ABCDEFGHJKMNPQRSTUVWXYZ";
//    NSUInteger base = baseCharacters.length;
//    NSMutableString *baseString = [NSMutableString new];
//    while (baseString.length < 12) {
//        uint64_t remainder = uid % base;
//        uid /= base;
//        NSString *baseCharacter = [baseCharacters substringWithRange:NSMakeRange(remainder, 1)];
//        [baseString insertString:baseCharacter atIndex:0];
//    }
//    NSLog(@"baseString: %@", baseString);
    uint16_t* key = get_key();
    NSLog(@"Key: '%hu%hu%hu%hu'",
          key[0], key[1], key[2], key[3]
    );

    NSString *data = @"Exifを付与しNSDataを作成するサンプルコード";
    NSData *udata = [data dataUsingEncoding:NSUTF8StringEncoding];
    NSMutableData *input = [[NSMutableData alloc] init];
    NSMutableData *encoded = [[NSMutableData alloc] init];
    NSMutableData *decoded = [[NSMutableData alloc] init];
    
    [input appendData:udata];
//    [input appendBytes:[udata bytes] length:[udata length]];
    NSLog(@"Message string is: '%@'", data);
    NSLog(@"Message string is: '%@'", input);
    
    
    Blowfish_Encode(key, input, encoded);
    NSLog(@"Encrypted message string is: '%@'", encoded);
    
    Blowfish_Decode(key, encoded, decoded);
    NSLog(@"Decrypted message string is: '%@'", [NSString stringWithUTF8String:[decoded bytes]]);
    
//    struct blowfish_result encoded = { .bytes = (uint8_t *) "", .length = 0 };
//    struct blowfish_result *encoded_ptr = &encoded;
//    Blowfish_Encode(key, data, encoded_ptr);
//    NSLog(@"trythis - Encrypted message string is: '%s'", blowfish_result_get_bytes(encoded_ptr));
//    
//    
//    struct blowfish_result decoded = { .bytes = (uint8_t *) "", .length = 0 };
//    struct blowfish_result *decoded_ptr = &decoded;
//    Blowfish_Decode(key, &encoded, &decoded);
//    NSLog(@"trythis - Decrypted message string is: %s", blowfish_result_get_bytes(&decoded));
//
//    free(decoded_ptr);
//    free(encoded_ptr);
    int a = 1;
}

- (void)encode:(CDVInvokedUrlCommand*)command
{
//    trythis();
    
//    NSLog(@"invoked Blowfish::encode");
//    id key = [command.arguments objectAtIndex:0];
    NSDate *methodStart = [NSDate date];
    NSString *jsdata = [command.arguments objectAtIndex:1];
    NSMutableData *unencoded = [[NSMutableData alloc] initWithData:[jsdata dataUsingEncoding:NSUTF8StringEncoding]];
//    NSLog(@"jsdata %s", unencoded.bytes);
    NSDate *methodFinish = [NSDate date];
    
    NSTimeInterval executionTime = [methodFinish timeIntervalSinceDate:methodStart];
    NSLog(@"encode copy executionTime = %f", executionTime);
    
    NSMutableData *encoded = [[NSMutableData alloc] init];
    
    
//    NSLog(@"Message string is: '%@' - out", [NSString stringWithUTF8String:[unencoded bytes]]);
    
//    const char *ckey = [key cStringUsingEncoding:NSASCIIStringEncoding];
//    NSData *encoded_data = [data dataUsingEncoding:NSMacOSRomanStringEncoding allowLossyConversion:YES];
//    uint8_t *cdata = (uint8_t *) [encoded_data bytes];
//    NSMutableString *output;
    
    uint16_t* key = get_key();


    NSString* item = [NSString stringWithFormat:@"Key: '%hu%hu%hu%hu'", key[0], key[1], key[2], key[3]];

//    NSLog(@"Key: '%hu%hu%hu%hu'", key[0], key[2], key[3], key[4]);
//    NSLog(@"Key: '%s'", ckey);
//    NSLog(@"Data: '%s'", cdata);
    
//    struct blowfish_result encoded = { .bytes = (uint8_t *)"", .length = 0 };
//    struct blowfish_result *encoded_ptr = &encoded;
//    Blowfish_Encode(key, unencoded, encoded);
    
    methodStart = [NSDate date];
    Blowfish_Encode(key, unencoded, encoded);
    methodFinish = [NSDate date];
    
    executionTime = [methodFinish timeIntervalSinceDate:methodStart];
    NSLog(@"encode Blowfish_Encode executionTime = %f", executionTime);
    
//    unencoded = nil;
    

//    NSLog(@"Encrypted message string is: '%@' - out", output);
//    NSLog(@"Encrypted message string is: '%@' - out", encoded);
    
//    NSString* encoded_bytes = [NSString stringWithFormat:@"%s", blowfish_result_get_bytes(encoded_ptr)];
    NSString* encoded_base_64 = [encoded base64EncodedString];
    
    
//    NSString *output = [[NSString alloc] initWithBytes:[encoded bytes] length:[encoded length] encoding:NSMacOSRomanStringEncoding];
    
    
    CDVPluginResult* pluginResult = nil;
    
    if (1 == 1) {
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:encoded_base_64];
    } else {
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:@"Arg was null"];
    }
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

- (void)decode:(CDVInvokedUrlCommand*)command
{
//    id key = [command.arguments objectAtIndex:0];
//    NSString *jsdata = (NSString*) [command.arguments objectAtIndex:1];
    
    
//    NSString *encoded_base_64 = [data base64DecodedString];
//    NSData *encoded_data = [encoded_base_64 dataUsingEncoding:NSMacOSRomanStringEncoding allowLossyConversion:YES];
//    const char *cdata = [encoded_data bytes];
    
    
//    NSMutableString *input;
//    NSMutableString *output;
    NSDate *methodStart = [NSDate date];
    NSString *jsdata = [command.arguments objectAtIndex:1];
//    NSData *encoded_data = [encoded_base_64 dataUsingEncoding:NSUTF8StringEncoding];
    NSMutableData *encoded = [[NSMutableData alloc] init];
    NSDate *methodFinish = [NSDate date];
    NSLog(@"encoded %s", encoded.bytes);
    
    NSTimeInterval executionTime = [methodFinish timeIntervalSinceDate:methodStart];
    NSLog(@"decode copy executionTime = %f", executionTime);

    NSMutableData *decoded = [[NSMutableData alloc] init];
    
    [encoded appendData:[jsdata base64DecodedData]];
    
//    [input appendString:[data base64DecodedString]];
//    NSLog(@"Encrypted message string is: '%@' - in", input);
//    NSLog(@"Encrypted message string is: '%@' - in", encoded);
    
    
//    NSLog(@"Encrypted message string is: '%s' - in", cdata);

//    const char *ckey = [key cStringUsingEncoding:NSASCIIStringEncoding];
    //uint8_t *cdata = (unsigned char *)[encoded_base_64 bytes];
    uint16_t *key = get_key();
//    NSLog(@"Decode Key: '%s' - in", ckey);
//    NSLog(@"Decode Key: '%c%c%c%c%c%c%c%c' - in", ckey[0], ckey[1], ckey[2], ckey[3], ckey[4], ckey[5], ckey[6], ckey[7]);
    
//    struct blowfish_result encoded = { .bytes = (unsigned char *) "", .length = encoded_data.length };
//    cats((char **) &encoded.bytes, cdata);
    //strlcpy(encoded.bytes, cdata, encoded_data.length + 1);
//    struct blowfish_result *encoded_ptr = &encoded;
//    NSLog(@"Encrypted message string is: '%s' - in ptr", blowfish_result_get_bytes(encoded_ptr));
//    struct blowfish_result decoded = { .bytes = (uint8_t *) "", .length = 0 };
//    struct blowfish_result *decoded_ptr = &decoded;
    
    methodStart = [NSDate date];
    Blowfish_Decode(key, encoded, decoded);
    methodFinish = [NSDate date];
    
    executionTime = [methodFinish timeIntervalSinceDate:methodStart];
    NSLog(@"decode Blowfish_Decode executionTime = %f", executionTime);
    //    NSLog(@"Decrypted message string is: %s", blowfish_result_get_bytes(decoded_ptr));
//    NSLog(@"Decrypted message string is: '%@'", output);
//    NSLog(@"Decrypted message string is: %d '%s' - out", [decoded length], [decoded bytes]);
    
    NSString *output = [[NSString alloc] initWithBytes:[decoded bytes] length:[decoded length] encoding:NSUTF8StringEncoding];
//    NSData *dt = [output dataUsingEncoding:NSMacOSRomanStringEncoding];
//    NSString *output_utf8 = [[NSString alloc] initWithData:dt encoding:NSUTF8StringEncoding];
//    NSString* encoded_base_64 = [output_utf8 base64EncodedString];
//    NSString *based_output = [output base64DecodedString];
//    NSLog(@"Decrypted message string is: '%@' - out", output);
    
    CDVPluginResult* pluginResult = nil;
    
    if (1 == 1) {
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:output];
    } else {
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:@"Arg was null"];
    }
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

@end
