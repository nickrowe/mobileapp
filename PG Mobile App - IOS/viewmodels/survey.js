(function (global) {
    var app = global.app = global.app || {},
    	save_btn,
    	current_survey,
    	view,
    	save_interval,
    	previous_responses_string = '',
    	status = 'connected';
	
	function getResponseValue(response, answer_id, key){
		if(typeof response != 'undefined' && app.checkNested(response, answer_id, key)){
			return response[answer_id][key];
		}else{
			return '';
		}
	}
	
	var render = {
		date: function(question, response){
			var template_content = $('#survey-date-template').html();
			var template = kendo.template(template_content);
			
			var answer_text = getResponseValue(response, question.answers[0].id, 'answer_text');
			
            var d = new moment(answer_text);
            var formattedDate;
            
            if (d.isValid()) {
                formattedDate = d.format('YYYY-MM-DD'); 
            } else {
                formattedDate = ''; 
            }
            
			var data = {
				answer_id: question.answers[0].id,
				key: 'answer_text',
				value: formattedDate
			};
			
			return {
				template: template,
				data: data
			};
		},
		length: function(question, response){
			var template_content = $('#survey-length-template').html();
			var template = kendo.template(template_content);
			
			var data = {
				answer_id: question.answers[0].id,
				key: 'answer_text',
				feet_value: '',
				inches_value: ''
			};
			
			var answer_text = getResponseValue(response, question.answers[0].id, 'answer_text');
			if(answer_text){
				data.feet_value = parseInt(answer_text / 12);
				data.inches_value = answer_text % 12;
			}
			
			return {
				template: template,
				data: data
			};
		},
		weight: function(question, response){
			var template_content = $('#survey-weight-template').html();
			var template = kendo.template(template_content);
			
			var data = {
				answer_id: question.answers[0].id,
				key: 'answer_text',
				value: getResponseValue(response, question.answers[0].id, 'answer_text'),
				unit_name: "answers['"+question.id+"']['"+question.answers[0].id+"'][survey_unit_id]",
				unit_value: getResponseValue(response, question.answers[0].id, 'survey_unit_id')
			};
			
			return {
				template: template,
				data: data
			};
		},
		checkbox: function(question, response){
			var template_content = $('#survey-checkbox-template').html();
			var template = kendo.template(template_content);
			
			var data = {
				answers: []
			};
			
			for (var i = 0; i < question.answers.length; i++) {
				var answer = question.answers[i];
				
				data.answers.push({
					question_id: question.id,
					answer_id: answer.id,
					key: 'answer_enum_id',
					response_value: getResponseValue(response, answer.id, 'answer_enum_id'),
					text: answer.value,
					unique_id: app.uniqueId()
				});
			}
			
			return {
				template: template,
				data: data
			};
		},
		radio: function(question, response){
			var template_content = $('#survey-radio-template').html();
			var template = kendo.template(template_content);
			
			var data = {
				answers: []
			};
			
			for(var i = 0; i < question.answers.length; i++){
				var answer = question.answers[i];
				
				data.answers.push({
					question_id: question.id,
					answer_id: answer.id,
					key: 'answer_enum_id',
					response_value: getResponseValue(response, answer.id, 'answer_enum_id'),
					text: answer.value,
					unique_id: app.uniqueId()
				});
			}
			
			return {
				template: template,
				data: data
			};
		},
		textarea: function(question, response){
			var template_content = $('#survey-textarea-template').html();
			var template = kendo.template(template_content);
			
			var data = {
				answer_id: question.answers[0].id,
				key: 'answer_text',
				value: getResponseValue(response, question.answers[0].id, 'answer_text')
			};
			
			return {
				template: template,
				data: data
			};
		}
	};
	
	function formatSurveyForm(form){
		//find all date fields and format it correctly
		form.find('.date-field').each(function(){
			var date = $(this).find('[type=text]').val();
            //console.log(date);
			//date = $.datepicker.formatDate('yy-mm-dd', $.datepicker.parseDate('mm/dd/yy', date));
			
			$(this).find('[type=hidden]').val(date);
		});
		
		//find all length fields and combine feet and inches
		form.find('.length-field').each(function(){
			var feet = +$(this).find('.feet').val();
			var inches = +$(this).find('.inches').val();
			
			inches += feet * 12;
			
			$(this).find('.feet_inches').val(inches || '');
		});
	}
	
	function getResponses(form){
        var responses = {};
		form.find('[data-answer_id]').each(function(){
			var question_id = $(this).data('question_id');
			var answer_id = $(this).data('answer_id');
			var key = $(this).data('key');
			var value = '';
			
			if(typeof responses[question_id] == 'undefined'){
				responses[question_id] = {};
			}
			if(typeof responses[question_id][answer_id] == 'undefined'){
				responses[question_id][answer_id] = {};
			}
			
			if($(this).attr('type') == 'checkbox' || $(this).attr('type') == 'radio'){
				value = $(this).is(':checked') ? 1 : 2;
			}else{
				value = $(this).val();
			}
			
			responses[question_id][answer_id][key] = value;
		});
        
        return responses;
    }
    
    function submitSurveyForm(form){
		if (app.survey.submitting) {
			return;
        } else {
			app.survey.submitting = true;
        }
		
		var survey_id = app.pages.data.survey.definition.id;
		
		//get new user responses
        var responses = getResponses(form);
		var responses_string = $.param({answers: responses});
        
		save_btn.css('opacity', .5).text('Saving...');
		
		var req = new app.Request({
			url: 'survey/' + survey_id,
			data: responses_string,
			type: 'PUT',
        });
		
		req.done(function (e) {
			app.survey.submitting = false;
            save_btn.css('opacity', 1).text('Save');
        })
		.fail(function (e) {
			app.survey.submitting = false;
			app.showError(app.messages.offlineGeneral, 'Unable to Save').then(function (e) {
				save_btn.css('opacity', 1).text('Save');
            });
        })
		.always(function (e) {
            previous_responses_string = responses_string;
            current_survey.responses = responses;
            app.setData('data', app.data);
        });
	}
    
    function updateStatus(new_status){
        var form = view.content.find('form');
        
        //just always do this (regardless if it has changed)
        if(new_status === 'disconnected'){
            save_btn.css('opacity', .5).html('Disconnected from Server');
            form.find(':input').prop('disabled', true);
        } else {
            form.find(':input').prop('disabled', false);
        }
        
        if(status === new_status){
            return;
        }
        
        status = new_status;
    }
	
	//would be more efficient to create an id -> survey lookup first and use that (but how many surveys can there be)
	function getSurvey(survey_id) {
		for(var i = 0; i < app.data.surveys.length; i++) {
			if(app.data.surveys[i].definition.id == survey_id) {
				return app.data.surveys[i];
			}
		}
	}
    
    function prepSurvey(form){
        formatSurveyForm(form);
        previous_responses_string = $.param({answers: getResponses(form)});
    }
    
    function initSurvey(survey_dom, survey_id){
		if(typeof survey_id != 'undefined' && typeof app.data != 'undefined' && typeof app.data.surveys != 'undefined' && app.data.surveys.length){
            current_survey = getSurvey(survey_id);
			app.pages.set('data.survey', current_survey);
            prepSurvey(survey_dom);
		}
    }
	
    app.survey = {
		submitting: false,
		resendForm: function () {
			console.log('hide');
			$('#survey-form').submit();
        },
		//intervals: {
		//	save: function () { setInterval(app.survey.resendForm, 5000); }
        //},
		//onlineOfflineEvents: {
		//	offline: false,
		//	online: false
        //},
		hide: function () {
			app.survey.resendForm();
			//clearInterval(save_interval);
            //document.removeEventListener("online", app.survey.resendForm, false);
        },
        init: function (e) {
            view = e.view;
			var content = view.content;
			
			var form = content.find('#survey-form');
			form.submit(function(evt){
                if (evt != null) {
					evt.preventDefault();
                }
                if (app.isInDemoAccount()) {
                    app.showError('Saving is disabled for demo account: '+app.loginService.viewModel.get('demo_email'));
                    return;
                }
				
				var form = $(this);
				
				formatSurveyForm(form);
				submitSurveyForm(form);
			});
			
            save_btn = e.view.footer.find('.survey-save');
			//save_btn.kendoTouch({
			//	tap: function(e){
            //        if(app.isInDemoAccount()){
            //            app.showError('Saving is disabled for demo account: '+app.loginService.viewModel.get('demo_email'));
            //            return;
            //        }
                    
			//		form.submit();
			//	}
			//});
            
            //var fake_footer = e.view.content.find('.fake-footer');
            //fake_footer.parent().parent().append(fake_footer);
            
            //initSurvey(content.find('#survey-form'));
            //$(document).on('app_data_set', function(){
			//	console.log('app_data_set fired');
            //    initSurvey(content.find('#survey-form'));
            //});
            
            //FastClick.attach(e.view.content[0]);
        },
		beforeShow: function (e) {
			if(!app.isLoggedIn()){
				e.preventDefault();
				app.application.navigate('views/account/login.html');
			}
			app.application.showLoading();
		},
		afterShow: function (e) {
			var content = e.view.content;
			app.layout.createMenu(e.view.header);
            //var form = e.view.content.find('#survey-form');
			//form.height(form.height());
            initSurvey(content.find('#survey-form'), e.view.params.survey_id);
			app.application.hideLoading();
			
            //document.addEventListener("online", app.survey.resendForm, false);
		},
		renderField: function(question, response){
            var render_response;
            
			switch(question.type){
				case 'DATE':
					render_response = render.date(question, response);
				break;
				
				case 'LENGTH':
					render_response = render.length(question, response);
				break;
				
				case 'WEIGHT':
					render_response = render.weight(question, response);
				break;
				
				case 'CHECKBOX':
					render_response = render.checkbox(question, response);
				break;
				
				case 'RADIO':
					render_response = render.radio(question, response);
				break;
				
				case 'TEXTAREA':
					render_response = render.textarea(question, response);
				break;
				
				default:
					return '';
				break;
			}
			
			render_response.data.question_id = question.id;
			
			return render_response.template(render_response.data);
		}
    };
})(window);