All files converted to max width of 100px, and then png's are crushed:
    
    cp /pathway/report/public/images/templates/fit_report/articles/* /pathway/mobile-api/public/media/images/fit_report_images/
    du -sh .
    for i in *; do convert $i -resize '100x' $i; done;
    du -sh .
    for i in *.png; do pngquant -f --speed 1 --quality 90 $i -o $i; done;
    du -sh .
