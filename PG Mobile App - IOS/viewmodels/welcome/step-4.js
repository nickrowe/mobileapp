
(function (global) {
    var Model,
    	service,
        app = global.app = global.app || {};

    Model = function () {
        return kendo.observable({
    	});
    };

    service = {
		beforeShow: function(e) {
            //TODO if they are already have logged in / registered before...
		},
		init: function(e) {
            //FastClick.attach(e.view.content[0]);
		},
        show: function (e) {
    		app.application.hideLoading();
            var viewModel = service.viewModel;
            app.layout.fillCenter(e.view.element);
			//app.resizeView('step-4', service, e.view.element);
            
            app.api.conciergeMatchAdvanced(
                e.view.params.name,
                e.view.params.date_of_birth,
                e.view.params.postal,
                e.view.params.accession_no
            ).done(function (resp) {
                if (resp.status && resp.count > 0) {
                    app.api.sync().then(function () {
						if ($('#categories_results_list_accordion').getKendoPanelBar()) {
							$('#categories_results_list_accordion').getKendoPanelBar().destroy();
			            }
						app.categories_results_list.buildAccordion();
    					app.application.navigate('views/welcome/step-7.html?message=' + encodeURIComponent(app.messages.reportFound));
                    });
                } else {
				    app.application.navigate('views/welcome/step-5.html');
                }
            }).fail(function (resp) {
                if (navigator.network.connection.type == Connection.NONE) {
					app.showError(app.messages.offlineReporting).then(function (e) {
						app.application.navigate('#lifestyle-view');
                    });
					return;
                }
				
				app.application.navigate('views/welcome/step-5.html');
            });
        },
        viewModel: new Model()
    };
    app.welcomeStep4Service = service;
})(window);