(function (global) {
    var app = global.app = global.app || {};

    function utf8_to_b64( str ) {
        return window.btoa(encodeURIComponent( escape( str )));
    }

    function b64_to_utf8( str ) {
        return unescape(decodeURIComponent(window.atob( str )));
    }
    
    function get_blowfish() {
        if(typeof Blowfish !== 'undefined'){
            if (device.platform === 'Android' && typeof Blowfish.Blowfish !== 'undefined') {
                return new Blowfish.Blowfish();
            }
            else if(device.platform === 'iOS'){
                return {
                    encode: function(success, error, key, data) {
                        cordova.exec(success, error, "Blowfish", "encode", [key, data]);
                    },
                    decode: function(success, error, key, data) {
                        cordova.exec(success, error, "Blowfish", "decode", [key, data]);
                    }
                };
            }
        }
        
        //alert((typeof Blowfish)+' -- '+device.platform);
        
        //create a fake Blowfish Class instance that does no encryption
        return {
            decode: function(success, fail, key, value){
                success(value);
            },
            encode: function(success, fail, key, value){
                success(value);
            }
        };
    }
    
    app.datastoreInit = function(){
        //var store_type = 'localStorage';
    	var store_type = 'sql';
    	//var store_type = 'global';
        var synchronous_keys = [
        	'data_date', 'last_activity_date', 'email', 'remember',
        	'used_before', 'force_no_reports', 'users'
        ];		//when store_type is 'sql', use localStorage for these fields
    	
    	var DS_Local = {
        	set: function(key, value){
                //app.log('DS_Local set', arguments);
    			localStorage.setItem(key, value);
    		},
    		get: function(key){
                //app.log('DS_Local get', key, localStorage.getItem(key));
    			return localStorage.getItem(key);
    		},
            remove: function(key){
    			localStorage.removeItem(key);
    		}
        };
    	
        var DS_Global = {
    		set: function(key, value){
    			app.GLOBAL[key] = value;
    		},
    		get: function(key){
    			return typeof app.GLOBAL[key] != 'undefined' ? app.GLOBAL[key] : null;
    		},
    		remove: function(key){
    			delete app.GLOBAL[key];
    		}
        };
        
    	var store;	//gets defined/initialized below
    	var DS_SQL = {
            set: function(key, value){
                //console.log('DS_SQL set', key, value);
                var exists = false;
                //alert('store ' + key);

				function storeValue(value){
                    //console.log("Encoded '" + value + "'");
                	//alert('storeValue ' + key);
            
                    store.transaction(function (tx) {
                		//alert('transstore.transactionaction ' + key);
                        // See if the key exists
                        var sql = 'SELECT storage_key FROM storage WHERE storage_key = ?';
                        tx.executeSql(sql, [key], function (tx, result) {
                            if (result.rows.length > 0) {
                                exists = true;
                            }
                            // Toggle between an update / insert
                            if (exists !== true) {
                                sql = 'INSERT INTO storage (storage_key, storage_value) VALUES(?, ?)';
                                tx.executeSql(sql, [key, value], function (tx, result) {
                                    //alert('INSERT INTO storage ' + key);
                                    if (key === 'password') {
                                        //alert(JSON.stringify(result));
                                    }
                                }, function (tx, error) {
                                    //alert('error2');
                                    alert('INSERT ERROR: ' + error.message);
                                });
                            } else {
                                sql = 'UPDATE storage SET storage_value = ? WHERE storage_key = ?';
                                tx.executeSql(sql, [value, key], function (tx, result) {
                                    //alert('UPDATE storage "' + key + '"');
                                    if (key === 'password') {
                                    	//alert('UPDATE storage value "' + value + '"');
                                        //alert(JSON.stringify(result));
                                    }
                                }, function (tx, error) {
                                    //alert('error3');
                                    alert('UPDATE ERROR: ' + error.message);
                                });
                            }
                        });
                    });
                }
                
                var b = get_blowfish();
                b.encode(function (encoded) {
                    storeValue(encoded);
                }, function (err) {
                    //alert('error1');
                    alert("ENCODE ERROR" + err);
                }, "", value);
            },
            get: function (key, func) {
                //app.log('DS_SQL get', arguments);
                var sql = 'SELECT storage_value FROM storage WHERE storage_key = ?';
                //alert('getValue ' + key);
                store.transaction(function (tx) {
                    //alert('get store.transaction ' + key);
                    tx.executeSql(sql, [key], function (tx, result) {
                        //alert('get tx.executeSql ' + key);
                        if (result.rows.length > 0) {
                            var value = result.rows.item(0).storage_value;
                            
                            var b = get_blowfish();
                            b.decode(function (decoded) {
                                try {
                                    func(decoded.replace(/^\0+/, '').replace(/\0+$/, ''));
                                } catch (e) {
                                    alert(e);
                                }
                            }, function (error) {
                                // alert(err);
                                alert('DECODE ERROR: ' + typeof error);
                            }, "", value);
                        }else{
                            func(false);
                        }
                    }, function (tx, error) {
                        alert('GET ERROR: ' + error);
                    });
                });
            },
    		remove: function (key, func) {
    			var sql = 'DELETE FROM storage WHERE storage_key = ?';
    			store.transaction(function (tx) {
    				tx.executeSql(sql, [key], function (tx, result) {
    					if (result.rows.length > 0) {
    						func(result.rows.item(0).storage_value);
    					}
    				}, function (tx, error) {
    					alert('REMOVE ERROR: ' + error.message);
    				});
    			});
    		}
    	}
        
        var DS;
    	if(store_type == 'localStorage'){
    		DS = DS_Local;
    	}
    	else if(store_type == 'sql'){
            /* the sqliteplugin used by telerik is giving "disk I/O error"s, so we'll just use the native one when testing
            if(window.navigator.simulator === true || window.sqlitePlugin === undefined) {
                // For debuging in simulator fallback to native SQL Lite
                store = window.openDatabase('Pathway', '1.0', 'Pathway', 50 * 1024 * 1024);
            } else {
                store = window.sqlitePlugin.openDatabase({name: 'Pathway'});
                //console.log('using sqlitePlugin');
                //alert('using sqlitePlugin');
            }
            */
            store = window.openDatabase('Pathway', '1.0', 'Pathway', 50 * 1024 * 1024);
    		
            //create table if it does not exist
    		store.transaction(function (tx) {
    			tx.executeSql(
    				'SELECT COUNT(*) FROM storage', [], function (result) {},
    				function (tx, error) {
    					// Create Database
    					var sql = 'CREATE TABLE storage (' +
    						  'storage_key VARCHAR(255) UNIQUE,' +
    						  'storage_value TEXT' +
    						')';
    					tx.executeSql(sql, [], function (result) {});
    				}
    			);
    		});
            
    		DS = DS_SQL;
    	}
    	else if(store_type == 'global'){
    		app.GLOBAL = {};
    		DS = DS_Global;
    	}
        
        app.encrypt = function(value_string){
            var encrypted = value_string;
            //console.log('about to encrypt');
            //var encrypted = CryptoJS.AES.encrypt(value_string, CryptoJS.SHA256(app.getUUID()).toString()).toString();
            //console.log('encrypted');
            return encrypted;
        }
        app.decrypt = function(value_string){
            var decrypted = value_string;
            //console.log('about to decrypt');
            //var decrypted = CryptoJS.AES.decrypt(value_string, CryptoJS.SHA256(app.getUUID()).toString()).toString(CryptoJS.enc.Utf8);
            //console.log('decrypted');
            return decrypted;
        }
    	app.setData = function(key, value){
            //alert(store_type);
            //console.log('setData', arguments);
            var value_string = JSON.stringify(value);
            value_string = app.encrypt(value_string);
    		
            if(store_type == 'sql' && $.inArray(key, synchronous_keys) !== -1){
                return DS_Local.set(key, value_string);
            }else{
            	return DS.set(key, value_string);
            }
        }
        
        app.getData = function(key, func){
            // alert(store_type);
            //SQL is asynchronous
            //alert(store_type);
            if(store_type == 'sql'){
            	//alert($.inArray(key, synchronous_keys) === -1);
                if($.inArray(key, synchronous_keys) === -1){
                	DS.get(key, function(value_string) {
                        // alert("value_string length:" + typeof value_string);
                        var value = null;
                        try{
                            value = app.decrypt(value_string);
                			value = value ? JSON.parse(value) : '';
                		}
                		catch(e){
                			app.showError(e);
                		}
                        
                        if(typeof func === 'function'){
                        	func(value);
                        }
                    });
                    
                    return;
                }else{
                    var value_string = DS_Local.get(key);
                }
            }else{
                var value_string = DS.get(key);
            }
    		
            //app.log('value_string', value_string);
            var value = null;
    		try{
                value = app.decrypt(value_string);
                //app.log('decrypted value', value);
    			value = JSON.parse(value);
    		}
    		catch(e){
    			app.showError(e);
    		}
            
            return value;
        }
    	app.removeData = function(key){
            if(store_type == 'sql' && $.inArray(key, synchronous_keys) !== -1){
                return DS_Local.remove(key);
            }else{
            	return DS.remove(key);
            }
        }
    }
})(window);