(function (global) {
    var app = global.app = global.app || {},
		Model,
		service;
	
	var blank_model = {
		validate: function (e) {
			e.preventDefault();
			var sendCopyToPhysician = service.viewModel.get('sendCopyToPhysician');
			
			var physicianEmail = service.viewModel.get('physicianEmail');
			
			var referral_id = service.viewModel.get('referral_id');
			var report_id = service.viewModel.get('report_id');
			
			var validator = $("#consult-not-eligible-form").kendoValidator().data("kendoValidator");
			var recipients = [];
			recipients.push(physicianEmail);
			
			var data = {
				"outcome": "not-candidate",
				"recipients": recipients
			}

			var addToQueue = function () {
				app.queue.addItemFromParams(
					/* type */    app.queue.consts.saveConsult, 
					/* url */     'referrals/' + encodeURIComponent(referral_id) + '/submit',
					/* data */    JSON.stringify(data),                                               
					/* options */ {
						type: 'POST',
						contentType: 'application/json'
	                }                          
				);
	        };
			
			console.log(validator.validate());
			
			var sendImmediateRequest = true;
			if (validator.validate()) {
				if (navigator.network.connection.type == Connection.NONE) {
					addToQueue();
					app.showError(app.messages.offlineConsulting, 'Notice').then(function () {
						app.application.navigate('#views/reports.html');
                    });
					sendImmediateRequest = false;
                }
            } else {
				sendImmediateRequest = false;
				app.showError('Sorry, the email entered for the physician is not a valid email address.', 'Invalid Email');
            }
			
			if (sendImmediateRequest) {
				app.application.showLoading();
				app.api.submitReferral(referral_id, data.outcome, data.recipients).then(function (data, textStatus, jqXHR) {
					console.log('consult - good', data, textStatus, jqXHR);
					if (!data.status) {
						addToQueue();
						app.showError(app.messages.offlineConsulting, 'Notice').then(function () {
							app.application.navigate('#views/reports.html');
                        });
		            } else {
						app.showNotice("Thank you, your request has been sent.").then(function () {
							app.application.navigate('#views/reports.html');
                        });
                    }
		        }).fail(function (jqXHR, textStatus, errorThrown) {
					console.log('consult - fails', jqXHR, textStatus, errorThrown);
					addToQueue();
					app.showError(app.messages.offlineConsulting, 'Notice').then(function () {
						app.application.navigate('#views/reports.html');
                    });
		        }).always(function () {
					app.application.hideLoading();
                });
            }
        },
		cancel: function (e) {
			e.preventDefault();
			app.application.navigate('#views/reports.html');
        },
		sendCopyToPhysician: false,
		physicianEmail: '',
		email: ''
    };
	
	Model = function () {
        return kendo.observable(blank_model);
    };

    service = {
        init: function (e) {
			FastClick.attach(e.view.content[0]);
        },
		beforeShow: function (e) {
		},
        show: function (e) {
			app.layout.createMenu(e.view.header);
			e.view.scroller.reset();
			
			service.viewModel.set('sendCopyToPhysician', false);
			service.viewModel.set('physicianEmail', '');
			service.viewModel.set('email', '');
			
			var referral_id = e.view.params.referral_id;
			var report_id = e.view.params.report_id;
			var user = app.getUser(app.getData('email'));
			referrals = user.get('referrals');
			service.viewModel.set('referral_id', referral_id);
			service.viewModel.set('report_id', report_id);
        },
		afterShow: function (e) {
        },
		hide: function (e) {
        },
		viewModel: new Model()
    };
	
	app.consultNotEligibleService = service;
})(window);