(function (global) {
    var app = global.app = global.app || {},
    	view,
        touchPoint = {};
    
    function updateNoResultsDiv(){
        if(app.pages.data.categories.length){
			view.content.find('.no-results').hide();
		}else{
			view.content.find('.no-results').show();
		}
    }

    app.categories = {
		touch: null,
        init: function (e) {
            view = e.view;
            
            $(document).on('app_data_set', function(){
                view.scroller.reset();
                updateNoResultsDiv();
            });
            //console.log('app.categories.init called');
            
			//var fast_click = FastClick.attach(view.content[0]);
            //fast_click.destroy();
            //FastClick.attach(view.content[0]);
			
			var touchPoint = {
				x: 0,
				y: 0
            };
			
			var userEvents = new kendo.UserEvents($('#lifestyle-view .tiles-container'), {
                press: function(e) {
                    touchPoint = {
                        y: e.touch.y.client,
                        x: e.touch.x.client,
						time: new Date().getTime()
                    };
				},
                release: function(e) {
					var deltaPoint = {
                        y: e.touch.y.client,
                        x: e.touch.x.client,
						time: new Date().getTime()
                    };
					
                    if (Math.abs(touchPoint.y - deltaPoint.y) < 10 && Math.abs(touchPoint.x - deltaPoint.x) < 10) {
	                    var li = $(e.event.target).closest('li');
	                    var categoryId = li.data('category-id');
	                    var result = li.data('result');
						global.current_category_id = null;
						document.activeElement && document.activeElement.blur();
						document.activeElement = null;
						if (categoryId === null) {
							return;
                        }
						
						setTimeout(function () {
		                    if (result) {
								app.application.navigate('#categories_results_list?category_id=' + categoryId);
		                    } else {
		                        app.application.navigate('#views/category-info.html?category_id=' + categoryId);
		                    }
                        }, 10);
						
						e.event.preventDefault();
                    }
                }
            });
			
        },
		beforeShow: function (e) {
            var loggedIn = app.isLoggedIn();
			if (!loggedIn) {
				e.preventDefault();
				app.application.navigate('views/account/login.html');
                return;
			}
            var user = app.getUser(app.getData('email'));

            var agreed_version = user.get('agreement_version');
            if (agreed_version == null || agreed_version != app.version) {
                navigator.splashscreen.hide();
                app.showEULA(false);
                e.preventDefault();
            }
		},
        show: function(e){
            global.current_category_id = null;
            updateNoResultsDiv();
			app.layout.createMenu(e.view.header);
            navigator.splashscreen.hide();
			
			//if (app.categories.touch != null) {
			//	if (app.categories.touch.getKendoTouch() != null) {
			//		app.categories.touch.getKendoTouch().destroy();
			//		app.categories.touch.each(function (i, e, c) {
			//			$(e).removeData('role');
			//			$(e).removeAttr('data-role');
            //        });
            //    }
			//	app.categories.touch = null;
			//}
			
			
			//TODO Marketing slide show....
            var user = app.getUser(app.getData('email'));
			var last_show = parseInt(user.get('marketing_slide_last_show'));
			if (!isFinite(last_show)) {
				last_show = 0;
            }
			var show_marketing = new Date().getTime() - last_show > app.marketing_timeout;
			var keepMeUpdated = user.getPreference('com.pathway.marketing.show-promotional-information');
			
			if (show_marketing && (keepMeUpdated === true || keepMeUpdated == null) && typeof app.data.marketing !== 'undefined') {
				var report_ids = app.data.marketing.reports;
				var last_index = parseInt(user.get('marketing_slide_index'));
				if (!isFinite(last_index)) {
					last_index = 0;
                }
				console.log('last_index', last_index, user.get('marketing_slide_index'));
				var current_index = last_index
				
				if (current_index >= report_ids.length) {
					current_index = 0;
                }
				
				var report_id = report_ids[current_index];
				var item = null;
				for (var key in app.data.reports) {
					if (app.data.reports[key].id == report_id) {
						item = app.data.reports[key];
						break;
                    }
                }
				
				if (item != null) {
					app.reports.showReportsPreview.bind(item)();
					
					user.set('marketing_slide_last_show', new Date().getTime());
					user.set('marketing_slide_index', current_index + 1);
                }
            }
        },
        afterShow: function (e) {
			//e.view.header.find(".km-tabstrip").data("kendoMobileTabStrip").switchTo('#lifestyle-view');
			$('[data-layout="layout-menu-tabs"] a[data-role="tab"]:nth-child(2)').removeClass('km-state-active');
			$('[data-layout="layout-menu-tabs"] a[data-role="tab"]:nth-child(1)').addClass('km-state-active');
        }
    };
})(window);