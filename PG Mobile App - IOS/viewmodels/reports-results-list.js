(function (global) {
    var app = global.app = global.app || {},
    	content,
    	scroller,
    	panel_bar,
		can_activate = null,
		last_url = null;
    
    function buildAccordion(){
        var collapse_duration = 200;
        
        var template = kendo.template($('#reports_results_list_template').html());
        var result = template({});
        
        $('#reports_results_list_accordion_wrapper').html(result);
        
        try{
        	kendo.bind($('#reports_results_list_accordion'), app.pages.data);
        }catch(e){
            app.genericError();
            return;
        }
        
        function getMaxScroll(content) {
            var height = $('#reports_results_list_accordion').height();
            var height2 = content.height();

            if (height < height2) {
                //--  Don't overscroll!
                return 0;
            } else {
                return height - content.height(); //height ? height - content.height() : 0;
            }
        }
        
		panel_bar = $('#reports_results_list_accordion').kendoPanelBar({
			expandMode: 'multiple',
            animation: {
                collapse: {
                    duration: collapse_duration
                }
            },
            collapse: function(){
                //wait for collapse to finish
                setTimeout(function(){
                	//make sure entire accordion is visible
                    //if(content.height() > $('#reports_results_list_accordion').height()){
                    //    scroller.animatedScrollTo(0, 0);
                    //}
                    
                    //make sure there's no white space at bottom
                    var max_scroll = getMaxScroll(content);
                    var max_scroll = Math.max(max_scroll, 0);
                    if(scroller.scrollTop > max_scroll) {
                        scroller.animatedScrollTo(0, -max_scroll);
                    }
                    
                    //scroller.contentResized();
                }, collapse_duration + 300);
            },
            expand: function (evt) {
				if (!can_activate) {
					evt.preventDefault();
					return;
                }
                setTimeout(function () {
                    var ul = $(evt.item).closest('ul');
                    var lis = ul.find('> li');
                    var max_scroll = getMaxScroll(content);
                    var offsetTop = Math.min(evt.item.offsetTop, max_scroll);
                    var offsetTop = Math.max(offsetTop, 0);
                    scroller.animatedScrollTo(0, -1 * offsetTop);
                }, collapse_duration + 300);
            }
		}).data('kendoPanelBar');
        
        //if there is only one accordion option, open it
        if($('#reports_results_list_accordion > li').size() == 1){
            panel_bar.expand($('#reports_results_list_accordion li:first')[0], false);
        }
        
        var max_scroll = $('#reports_results_list_accordion').height() > content.height() ? $('#reports_results_list_accordion').height() - content.height() : 0;
        //console.log(max_scroll);
        if(scroller.scrollTop > max_scroll){
            //scroller.animatedScrollTo(0, -max_scroll);
            scroller.scrollTo(0, -max_scroll);
        }
    }

    app.reports_results_list = {
        init: function (e) {
            content = e.view.content;
            scroller = e.view.scroller;
            
            //rebuild accordion every time it changes. (Can't use normal binding to reflect change because kendoPanelBar removes binding)
            app.pages.bind('change', function(e){
                //only need to rebuild accordion if the reports_accordions array changes
                if(e.field == 'data' || e.field == 'data.reports_accordions'){
                	buildAccordion();
                }
            });
            
            
            //FastClick.attach(content[0]);
			
			var touchPoint = {
				x: 0,
				y: 0
            };
			
			var userEvents = new kendo.UserEvents($('#reports_results_list_accordion_wrapper'), {
                press: function(e) {
                    touchPoint = {
                        y: e.touch.y.client,
                        x: e.touch.x.client,
						time: new Date().getTime()
                    };
				},
                release: function(e) {
					var deltaPoint = {
                        y: e.touch.y.client,
                        x: e.touch.x.client,
						time: new Date().getTime()
                    };
					
                    if (Math.abs(touchPoint.y - deltaPoint.y) < 10 && Math.abs(touchPoint.x - deltaPoint.x) < 10) {
	                    var li = $(e.event.target).closest('li.reports_results_list_li_li');
						var share = $(e.event.target).closest('.share-icon');
						if (li.length > 0 && share.length === 0) {
		                    var result_id = li.data('result-id');
		                    var title = li.data('title');
							document.activeElement && document.activeElement.blur();
							document.activeElement = null;
							
							setTimeout(function () {
								app.application.navigate('#views/result.html?result_id=' + result_id + '&title=' + title);
	                        }, 50);
						}
						else if (share.length > 0) {
							app.result.share(e.event);
                        }
                    }
                }
            });
			
			//$('#reports_results_list .km-native-scroller').bind('scroll', function (e) {
			//	touchPoint = {
			//		x: 0,
			//		y: 0
            //    };
            //});
        },
        beforeShow: function (e) {
			if(!app.isLoggedIn()){
				e.preventDefault();
				app.application.navigate('views/account/login.html');
			}
			
			if(typeof app.data == 'undefined' || typeof app.data.applicable == 'undefined'){
				app.application.navigate('#lifestyle-view');
			}
			
			console.log(document.location.href);
        },
        show: function (e) {
			app.layout.createMenu(e.view.header);
            e.view.header.find('[data-role=view-title]').text(e.view.params.title);
			
			var report_id = e.view.params.report_id;
			
			if (typeof app.data.applicable.reports.orders[report_id] === 'undefined') {
				app.application.navigate('#views/reports.html');
				e.preventDefault();
				return;
            }
			
			var order_id = app.data.applicable.reports.orders[report_id][0];	//just get the first one for now
			var order = app.data.results.by_order[order_id];
            
            //use app.data.applicable.categories to get the order of the categories for order.categories
            var categories_ids_to_use = Object.keys(order.categories);
            for(var i = 0; i < categories_ids_to_use.length; i++){
                categories_ids_to_use[i] = parseInt(categories_ids_to_use[i]);
            }
            var categories_ids = app.data.applicable.categories.filter(function(n){
                return (categories_ids_to_use.indexOf(n) != -1);
            });
            
			var categories = [];
			for(var i = 0; i < categories_ids.length; i++){
                var category_id = categories_ids[i];
				//var category = order.categories[category_id];
				var category = app.data.categories[category_id];
                var results_ids = app.data.results.by_order[order_id].categories[category_id];
                
                //add results to category
                category._results = [];
                for(var j in results_ids){
                    var result_id = results_ids[j];
                    //var result = order.results[result_id];
                    var result = app.data.results.user.results[result_id];
                    
                    if(typeof result !== 'undefined'){
                    	category._results.push(result);
                    }
                }
				
				categories.push(category);
			}
			
			//if it has changed at all
            if(JSON.stringify(app.pages.data.reports_accordions) != JSON.stringify(categories)){
				app.pages.set('data.reports_accordions', categories);
            }
			
			
		},
        afterShow: function (e) {
			can_activate = true;
			if (!last_url || last_url !== e.view.params.report_id) {
            	buildAccordion();
            }
			can_activate = false;
			last_url = e.view.params.report_id;
			
			setTimeout(function () {
				can_activate = true;
            }, 200);
        }
    };
})(window);