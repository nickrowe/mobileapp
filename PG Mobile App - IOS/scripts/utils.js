(function (global) {
    var app = global.app = global.app || {};

	/**
	 * $.unserialize
	 *
	 * Takes a string in format "param1=value1&param2=value2" and returns an object { param1: 'value1', param2: 'value2' }. If the "param1" ends with "[]" the param is treated as an array.
	 *
	 * Example:
	 *
	 * Input:  param1=value1&param2=value2
	 * Return: { param1 : value1, param2: value2 }
	 *
	 * Input:  param1[]=value1&param1[]=value2
	 * Return: { param1: [ value1, value2 ] }
	 *
	 * @todo Support params like "param1[name]=value1" (should return { param1: { name: value1 } })
	 * Usage example: app.log($.unserialize("one="+escape("& = ?")+"&two="+escape("value1")+"&two="+escape("value2")+"&three[]="+escape("value1")+"&three[]="+escape("value2")));
	 */
	(function($){
		$.unserialize = function(serializedString){
			var str = decodeURI(serializedString);
			var pairs = str.split('&');
			var obj = {}, p, idx;
			for (var i=0, n=pairs.length; i < n; i++) {
				p = pairs[i].split('=');
				idx = p[0];
				if (obj[idx] === undefined) {
					obj[idx] = unescape(p[1]);
				}else{
					if (typeof obj[idx] == "string") {
						obj[idx]=[obj[idx]];
					}
					obj[idx].push(unescape(p[1]));
				}
			}
			return obj;
		};
	})(jQuery);

    /* a little buggy on Android
	//This allows delegated events
	$(document)
		.on('touchstart', function(e){
			var touches = e.originalEvent.changedTouches;        //touches and changedTouches seem to be the same for touchstart
			var element = $(e.target);

			//if there's only one touch
			if(touches.length == 1){
				element.data({
					_clicking: true,
					_touch: {
						pageX: touches[0].pageX,
						pageY: touches[0].pageY
					}
				});
			}else{
				element.removeData(['_clicking', '_touch']);
			}
		})
		.on('touchend', function(e){
			var element = $(e.target);

			if(element.data('_clicking')){
				var touches = e.originalEvent.changedTouches;        //only changedTouches exist for touchend
				var start_touch = element.data('_touch');

				//if there's only one touch and it has not moved
				if(touches.length == 1 && (start_touch.pageX == touches[0].pageX && start_touch.pageY == touches[0].pageY)){
					element.trigger('kendoclick');
				}

				element.removeData(['_clicking', '_touch']);
			}
		});
    */

	app.uniqueId = (function(){
		var counter = 0;

		return function(){
			counter++;
			return 'u'+counter;
		}
	})();

    app.alert = function (title, fn, msg, btns) {
        if (fn == null) {
            fn = function () {};
        }
        if (typeof navigator.notification !== 'undefined' && typeof navigator.notification.alert !== 'undefined') {
            navigator.notification.alert(msg, fn, title, btns);
        } else {
            alert(msg);
            if (fn != null) {
                fn(1);
            }
        }
    };

    //console.log gets defined after application is initialized
    app.log = (function(){
        return function(){
            //console.log.apply(console, arguments);
        }

        /* I can't figure out a way to determine when console.log is replaced by the one that works in appbuilder. Otherwise, this would work with caching and all.
        var temp_arguments_array = [];
        var log_function;

        var good_log_function = function(){
        	console.log.apply(console, arguments);
        }
        var temp_log_function = function(){
            temp_arguments_array.push(arguments);
        }

        //seems to be defined, just doesn't work on phone...  if(typeof console.log === 'function'){
        if(typeof app.application === 'undefined'){
            log_function = temp_log_function;

            var log_checker = setInterval(function(){
                if(typeof app.inited !== 'undefined' && app.inited){
                    clearInterval(log_checker);

                    log_function = good_log_function;

                    for(var i = 0; i < temp_arguments_array.length; i++){
                        log_function.apply(null, temp_arguments_array[i]);
                    }
                }
            }, 50);
        }else{
            log_function = good_log_function;
        }

        return log_function;
        */
    })();

	app.getUUID = function(){
        return 'pathd43f4uy6w336';  //device.uuid is defined whenever it feels like it, so can't use...    + (typeof device != 'undefined' ? device.uuid : '25ff54u4y6w4y6w22f546');
	}

	//since query parameters (?foo=bar) are not available in the beforeShow event, make them available in a app.GET() function
	app.GET = function (param) {
		var params_string = window.location.hash.substring(window.location.hash.indexOf('?')+1);
		var params = $.unserialize(params_string);

		if (param) {
			return params[param];
		}

		//app.log(params);

		return params;
	}

	app.iframeClickThrough = function(e, iframe, view){
		var iframe_document = iframe[0].contentWindow.document;
		var x = e.originalEvent.pageX + view.scroller.scrollLeft;
		var y = e.originalEvent.pageY + view.scroller.scrollTop - view.header.height();

		var under_element = iframe_document.elementFromPoint(x, y);
		if(e.type == 'click' && under_element.nodeName.toLowerCase() == 'a' && under_element.href.length){
			//does not work under_element.click();
            //ditto $(under_element).click();

            if(under_element.href.indexOf('tel:') !== -1){
            	location.href = under_element.href;
            }
            else if(under_element.href.indexOf('http:') !== -1){
                app.externalURL(under_element.href);
            }
		}else{
			iframe_document.defaultView.jQuery(under_element).trigger(e.type);
		}
	}

    //override fastclick to do nothing on androids
	if (typeof FastClick !== 'undefined') {
	    var old_fastclick_attach = FastClick.attach;
	    FastClick.attach = function(element, options){
	        if(deviceIsAndroid){
	            return false;
	        }
	        
	        return old_fastclick_attach(element, options);
	        
	        //$(element).find('a[href]').off('touchstart touchend').on('touchend', function(e){
	        //    e.preventDefault();
	        //    e.stopImmediatePropagation();
	        //    e.stopPropagation();
	            
	        //    app.application.navigate($(this).attr('href'));
	            
	        //    return false;
	        //});
	    }
	}
    
    app.checkNested = function(obj /*, level1, level2, ... levelN*/) {
        var args = Array.prototype.slice.call(arguments),
          obj = args.shift();

        for (var i = 0; i < args.length; i++) {
            if (!obj.hasOwnProperty(args[i])) {
            	return false;
            }
            obj = obj[args[i]];
        }
        return true;
    }

    //this no longer works after an iframe is shown in the app at any time. UGH!!
    app.externalURL = function(url){
        //alert(url);

        if(url.indexOf('tel:') !== -1 || url.indexOf('mailto:') !== -1){
        	location.href = url;
            return;
        }
        else if(url.indexOf('http:') !== -1){
            if(app.checkNested(navigator, 'app', 'loadUrl')){
            	//Android
                navigator.app.loadUrl(url, { openExternal:true });
            }else{
                //iOS
                window.open(url, '_system');
            }
        }
    }

    app.scrollTo = function (event) {
        var ios = typeof kendo.mobile.application.os.ios !== 'undefined',
            android = typeof kendo.mobile.application.os.android !== 'undefined';
        if (android) {
            setTimeout(function () {
                var elem = $(event.target);
                var view = elem.closest('.km-view').getKendoMobileView();
                if (view.scroller != null) {
                    var currentScroll = view.scroller.element[0].scrollTop;
                    var viewportHeight = view.scroller.element[0].clientHeight;
                    var newScroll = Math.round(currentScroll + elem.position().top - (viewportHeight / 2));
                    view.scroller.animatedScrollTo(0, newScroll * -1);
                }
            }, 250);
        }
    };

    app.scrollTo2 = function (event, absolute) {
        var ios = typeof kendo.mobile.application.os.ios !== 'undefined',
            android = typeof kendo.mobile.application.os.android !== 'undefined';
        if (android) {
            setTimeout(function () {
                var elem = $(event.target);
                var view = elem.closest('.km-view').getKendoMobileView();
                if (view.scroller != null) {
					view.scroller.reset();
                    var viewportHeight = $(document.body).height();
					var offset;
					if (absolute) {
						offset = elem.offset().top - (viewportHeight / 2);
                    } else {
						offset = elem.position().top  + (viewportHeight / 2);
                    }
					
                    var newScroll = Math.round(offset);
                    view.scroller.animatedScrollTo(0, newScroll * -1);
                }
            }, 250);
        }
    };
    
    app.datePicker = function (event, container, field, viewModel, save, minDate, maxDate) {
        if (save == null) {
            save = function () {};
        }

        var y = $(container).offset().top + $(container).outerHeight();
        var x = ($(document.body).outerWidth() / 2);
        var ios = typeof kendo.mobile.application.os.ios !== 'undefined',
            android = typeof kendo.mobile.application.os.android !== 'undefined';

    	var dateInput = $(container).find('input[type=text]');
        var currentDate = new moment(dateInput.val());

        if (!currentDate.isValid()) {
        	currentDate = new moment(new Date((new Date()).getFullYear() - 10, 11, 31));
        }

        var presentedDate = currentDate.toDate();

        if (minDate == null) {
        	minDate = new Date(1865, 0, 31);
        }
        if (maxDate == null) {
        	maxDate = new Date((new Date()).getFullYear() - 10, 11, 31);
        }

        if (android) {
            // Android wants min and max in a different format
            minDate = minDate.getTime();
            maxDate = maxDate.getTime();
        }

        var options = {
            date: presentedDate,
            minDate: minDate,
            maxDate: maxDate,
            mode: 'date',
            y: y,
            x: x
        };

        datePicker.show(options, function(date) {
    		var d = new moment(date);
    		var formattedDate;
    		if (d.isValid()) {
    			formattedDate = d.format('YYYY-MM-DD');
    		} else {
    			formattedDate = '';
    		}
			
    		dateInput.val(formattedDate);
			
			if (field.indexOf('data.survey.responses') > -1) {
				var keys = field.match(/\d+/g);
				var answerId = keys[0]

				var key1 = 'data.survey.responses[' + keys[0] + ']';
				if (viewModel.get(key1) == null) {
					viewModel.set(key1, []);
                }

				var key2 = key1 + '[' + keys[1] + ']';
				if (viewModel.get(key2) == null) {
					viewModel.set(key2, {answer_text: ''});
                }
            }
			
    		viewModel.set(field, formattedDate);
            save(formattedDate);
        });
    };

    app.get18YearsAgo = function () {
        var d = new Date();
        d.setFullYear(d.getFullYear() - 18);
        d.setMonth(11);
        d.setDate(31);
        return d;
    };

	app.resizeView = function (href, service, element) {
		setInterval(function () {
			var oldH = service.viewModel.get('height');
			var newH = $(document.body).height();
			if (oldH != newH && oldH < newH && document.location.href.indexOf(href) > -1) {
        		app.layout.fillCenter(element);
            }

			service.viewModel.set('height', newH);

        }, 100);
    };
	
	app.hextoRGBA = function (value) {
	    hex = value.replace('#','');
	    r = parseInt(hex.substring(0,2), 16);
	    g = parseInt(hex.substring(2,4), 16);
	    b = parseInt(hex.substring(4,6), 16);
	    a = parseInt(hex.substring(6,8), 16) / 255;
		
		return 'rgba('+r+','+g+','+b+','+a+')';
    }

	if (global.start == null) {
        global.start = function () {};
    }
	if (global.addRow == null) {
        global.addRow = function () {};
    }
})(window);

/*
 * jQuery throttle / debounce - v1.1 - 3/7/2010
 * http://benalman.com/projects/jquery-throttle-debounce-plugin/
 * 
 * Copyright (c) 2010 "Cowboy" Ben Alman
 * Dual licensed under the MIT and GPL licenses.
 * http://benalman.com/about/license/
 */
(function(b,c){var $=b.jQuery||b.Cowboy||(b.Cowboy={}),a;$.throttle=a=function(e,f,j,i){var h,d=0;if(typeof f!=="boolean"){i=j;j=f;f=c}function g(){var o=this,m=+new Date()-d,n=arguments;function l(){d=+new Date();j.apply(o,n)}function k(){h=c}if(i&&!h){l()}h&&clearTimeout(h);if(i===c&&m>e){l()}else{if(f!==true){h=setTimeout(i?k:l,i===c?e-m:e)}}}if($.guid){g.guid=j.guid=j.guid||$.guid++}return g};$.debounce=function(d,e,f){return f===c?a(d,e,false):a(d,f,e!==false)}})(this);