(function (global) {
    var app = global.app = global.app || {};
    app.isLoggedIn = function () {
        var last_activity_date = app.getData('last_activity_date');
        
        var inactive = typeof last_activity_date === 'undefined' || last_activity_date < $.now() - (1000 * 60 * 60 * 5);
        
        //if the data is too old, and there has NOT been recent activity
        if (inactive) {
            //-- Dont log out!
            var rememberThePromise = app.getData('remember');
            if (!rememberThePromise) {
                app.removeData('password');
                return false;
        	}
        }
        
        app.setData('last_activity_date', $.now());
        return true;
    };
})(window);