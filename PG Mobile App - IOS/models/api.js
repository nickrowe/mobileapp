(function (global) {
    var app = global.app = global.app || {};
	
	/* Example:
		app.api.call({
			request: 'utils/reflector',
			type: 'PUT',
			data: {
				foo: 'bar'
			},
			onComplete: function(data){
				app.log(data);
			}
		});
	*/
    app.api = {
		call: function (options, retried) {
            if (retried == null) {
                retried = false;
            }
            
            var mocked = false;
            if (app.mock) {
                var url = options.request;
				if (options.mockable) {
					mocked = true;
                }
                else if (url == 'marketing/intro_slides') {
                    mocked = true;
                }
                else if (url == 'sync') {
                    mocked = true;
                }
                else if (url == 'concierge/get_display_defaults') {
                    mocked = true;
                }
                else if (url == 'concierge/extended_search') {
                    mocked = true;
                }
                else if (url == 'concierge/match_advanced') {
                    mocked = true;
                }
            }
            
            if (mocked === true) {
                if (app.mock_not_found) {
                    url = url + '_not_found';
                }
                else if (app.mock_failed) {
                    url = url + '_failed';
                }
            }
            
            //-- If mocked replace the gateway for localfiles
            var api_call_url = app.gateway + app.gateway_version + '/' +options.request;
            if (mocked) {
                api_call_url = 'mocks/' + options.request;
            }
            
            //-- If they are offline
            if (navigator.network.connection.type === Connection.NONE) {
                //-- Check to see if there is a cache first version
                var isCache = false;
                if (options.cacheable) {
                    var cache = window.localStorage.getItem('cache.' + options.request);
                    if (cache && options.onComplete != null) {
                        try {
                            isCache = true;
                            options.onComplete(JSON.parse(cache));
                        } catch (e) {
                        	options.onError(e);
                        }
                        return;
                    }
                }
                
                //-- Else check to see if there is a default version
                if (options.defaultable && !isCache) {
                	api_call_url = 'defaults/' + options.request;
                    return;
                }
            }
            
			var options = $.extend({}, {
				request: 'utils/reflector',
				type: 'GET',
				data: {},
                timeout: 10 * 1000,
                onComplete: function(){},
                onError: function(){}
			}, options);
			
            var ajaxOptions = {
				url: api_call_url,
				type: options.type,
                timeout: options.timeout,
				data: options.data
			};
            if (options.dataType) {
                ajaxOptions.dataType = options.dataType;
            }
            
			var api_call = $.ajax(ajaxOptions);
            
            function retryLogin (fn) {
                if (fn === null) {
                    fn = function () {};
                }
                var password;
                var email = app.getData('email');
                app.getData('password', function (password) {
                	//alert('password123: ' + password);
    				app.api.call({
    					request: 'auth/login',
    					type: 'POST',
    					data: {
    						email: email,
    						password: password
    					},
    					onComplete: function (data, textStatus, request) {
                            if(request.status == 401) {
								if (typeof app.application !== 'undefined') {
	                            	app.application.hideLoading();
	                                app.showError('You have been logged out. Please log back in.').done(function() {
	                                    app.loginService.clearData();
	                                    app.application.navigate('views/account/login.html');
	                                });
                                }
                                return;
                            } else {
                                fn();
                            }
    					},
                        onError: function(){
                            if (navigator.network.connection.type == Connection.NONE) {
                            	app.showError('Looks like your device is offline. Please try again when you have a network connection.');
                            } else {
                            	app.showError('There was a problem connecting to the server. Please try again soon.');
                            }
                        }
    				}, true);
                });
            }
			
			api_call
				.done(function (data, textStatus, request) {
                    if(request.status == 401 && !retried) {
                        retryLogin(function () {
                            options.onComplete(data, textStatus, request);
                        });
                        return;
                    } else if (request.status == 401) {
                        
                    } else {
						options.onComplete(data, textStatus, request);
                    }
					
				}).fail(function (request, textStatus, errorThrown) {
					if (typeof app.application !== 'undefined') {
                    	app.application.hideLoading();
					}
					
                    var data = {};
                    try {
                        data = JSON.parse(request.responseText);
						app.log('data:', data);
                    } catch (e) {}
                    
                    if (request.status == 401) {
                        retryLogin(function () {
                			var api_call2 = $.ajax({
                				url: app.gateway+app.gateway_version+'/'+options.request,
                				type: options.type,
                                timeout: options.timeout,
                				data: options.data
                			}).done(function (data, textStatus, request) {
                                options.onComplete(data, textStatus, request);
                            }).fail(function (request, textStatus, errorThrown) {
                                options.onError(data, textStatus, request);
                            });
                        });
                        return;
                    } else if (request.status == 401) {

                    } else {
                        if (navigator.network.connection.type == Connection.NONE) {
                			var isCache = false;
                            //-- Check to see if there is a cache first version
                            if (options.cacheable) {
                                var cache = window.localStorage.getItem('cache.' + options.request);
                                if (cache && options.onComplete != null) {
                                    try {
                                        isCache = true;
                                        options.onComplete(JSON.parse(cache));
                                    } catch (e) {
                                    	options.onError(e);
                                    }
                                    return;
                                }
                            }
                            //-- Else check to see if there is a default version
                            if (options.defaultable && !isCache) {
                                //-- just try again to see if it is still offline
                                app.api.call(options, true);
                                return;
                            }
                        }
                        
                    	options.onError(data, textStatus, request);
                    }
                    
				});
            
            if (app.emulatable_broken_connection && !retried) {
                var broken = (Math.random() * 2 | 0) + 1;
                if (broken === 1) {
                    navigator.network.connection.type = Connection.NONE;
                    setTimeout(function () {
                    	navigator.network.connection.type = Connection.WIFI;
                    }, 500);
                    api_call.abort();
                    return;
                }
            }
		},
		sync: function(){
			var deferred = $.Deferred();
            
            function complete(data){
                app.api.setAppData(data);
				app.setData('data', data);
				app.setData('data_date', $.now());
				app.setData('last_activity_date', $.now());
				
				
                //give the app some time to do all the data bindings (otherwise the app will be unresponsive until it's done)
                setTimeout(function(){
					deferred.resolve();
                }, 2000);
            }
            
			app.api.call({
				request: 'sync',
                dataType: 'json',
                timeout: 30 * 1000,	//could be a lot of data
				onComplete: function(data, textStatus, request){
					complete(data);
				},
                onError: function(){
                    if (navigator.network.connection.type == Connection.NONE) {
                    	app.showError('Looks like your device is offline. Please try again when you have a network connection.');
                    } else {
                    	app.showError('There was an error communicating with the server');
                    }
					deferred.reject();
                }
			});
			
			return deferred.promise();
		},
		
		//set the data to be used in the app. formats it correctly.
		// will be using underscore prefixes to avoid a future namespace collision
		setAppData: function (data) {
            var deferred = $.Deferred();
			app.api.resetSyncData();
			var pages_data = app.pages.data.toJSON();
			
			//pages_data.categories
			for(var i in data.applicable.categories){
				var category_id = data.applicable.categories[i]
                var category = data.categories[category_id];
                
                category._results = [];
                
				pages_data.categories.push(category);
			}
			
			//pages_data.reports
			for (var i in data.applicable.reports.ids) {
				var report_id = data.applicable.reports.ids[i];
                var report = data.reports[report_id];
                
				pages_data.reports.push(report);
			}
			
            /* need to change this to use data.applicable.categories instead to keep sort order
			//pages_data.categories_accordions
			for(var category_id in data.results.user.categories){
				var category = data.categories[category_id];
				
				for(var i in data.results.user.categories[category_id]){
					var results_id = data.results.user.categories[category_id][i];
                    
                    if(typeof category._results == 'undefined'){
                        category._results = [];
                    }
					
					category._results.push(data.results.user.results[results_id]);
				}
                
				pages_data.categories_accordions.push(category);
			}
            */
            //because we're now using data.applicable.categories, we can probably merge this with the loop above
            for(var i in data.applicable.categories){
				var category_id = data.applicable.categories[i]
                var category = data.categories[category_id];
                
                for(var i in data.results.user.categories[category_id]){
					var results_id = data.results.user.categories[category_id][i];
                    
                    if(typeof category._results == 'undefined'){
                        category._results = [];
                    }
					
					category._results.push(data.results.user.results[results_id]);
				}
                
				pages_data.categories_accordions.push(category);
			}
            
            /*
            //pages_data.data.orders
            var used_accession_numbers = [];
            for(var i in data.results.by_order){
                var order = data.results.by_order[i];
                
                if($.inArray(order.accession.number, used_accession_numbers) === -1){
    				pages_data.orders.push(order);
                    used_accession_numbers.push(order.accession.number);
                }
			}
            */
            pages_data.activations = data.activations;
            
			app.data = data;
			
			/*
			//pages.survey (lets just set it to the first one)
			pages_data.survey = data.surveys[0];
			*/
			app.pages.set('data', pages_data);
			
			$(document).trigger('app_data_set');
			
			if ($('#categories_results_list_accordion').getKendoPanelBar()) {
				$('#categories_results_list_accordion').getKendoPanelBar().destroy();
            }
			app.categories_results_list.buildAccordion();
            
            deferred.resolve();
            return deferred.promise();
		},
		resetSyncData: function(){
			//** Create an object that can be used easily with the app **
			//note: must create structure right away so Kendo can do all the bindings
			var pages_data = {
				categories: [],
				reports: [],
				
					categories_accordions: [],
					reports_accordions: [],		//based on an order
					category_info: {			//the currently active category (for showing it's info/view)
						view: ''				//must have this default view string because the html is rendered before the binding happens in app.category_info.beforeShow
					},
				
				survey: {
					definition: {
						name: '',
						sections: []
					}
				},
                
                orders: []
			};
			
			if(typeof app.pages == 'undefined'){
				app.pages = kendo.observable({
                    online: true,
                    offline: false,
                    saveSurvey: function (evt) {
                        $('#survey-form').submit();
                        if (app.isInDemoAccount()) {
                            // They cannot save so I cannot redirect them
                            return;
                        }
                        app.application.navigate('#lifestyle-view');
                    },
                    saveAssociateReport: function (evt) {
                        $('#associate-report-form').submit();
                    },
					selectCategory: function (evt) {
                    }
                });
				
				app.pages.bind('change', function (e) {
					if (e.field === 'data') {
						app.layout.syncSurveys();
                    }
	            });
			}
			app.pages.set('data', pages_data);
		},
        getUserDetails: function () {
            var deferred = $.Deferred();
			app.api.call({
				request: 'concierge/search_defaults',
				mockable: true,
				type: 'GET',
                dataType: 'JSON',
				onComplete: function (a, b, c) {
                    var user = app.getUser(app.getData('email'));
                    user.setMany(a);
                    
                    deferred.resolve(a, b, c);
                },
                onError: function (a, b, c) {
                    deferred.reject(a, b, c);
                }
			});
            
            return deferred.promise();
        },
        conciergeMatchSimple: function (date_of_birth, accession_no) {
            var deferred = $.Deferred();
            var data = {
                date_of_birth: (new moment(date_of_birth)).format('YYYY-MM-DD'),
                accession_no: accession_no.toUpperCase()
            };
            
			app.api.call({
				request: 'concierge/match_simple',
				data: data,
				type: 'POST',
				onComplete: function (a, b, c) {
                    deferred.resolve(a, b, c);
                },
                onError: function (a, b, c) {
                    deferred.reject(a, b, c);
                }
			});
            
            return deferred.promise();
        },
        conciergeMatchAdvanced: function (name, date_of_birth, postal, accession_no) {
            var deferred = $.Deferred();
            var data = {
                name: name,
                date_of_birth: (new moment(date_of_birth)).format('YYYY-MM-DD'),
                postal: postal,
                accession_no: accession_no.toUpperCase()
            };
            
			app.api.call({
				request: 'concierge/extended_search',
				data: data,
				type: 'POST',
				onComplete: function (a, b, c) {
                    deferred.resolve(a, b, c);
                },
                onError: function (a, b, c) {
                    deferred.reject(a, b, c);
                }
			});
            
            return deferred.promise();
        },
        addReport: function (accession_no, date_of_birth, activation_code) {
            var deferred = $.Deferred();
            var data = {
                date_of_birth: (new moment(date_of_birth)).format('YYYY-MM-DD'),
                accession_no: accession_no.toUpperCase()
            };
			
			new app.Request({
				url: 'user/associate',
				data: data,
				type: 'POST'
			})
			.done(function (a, b, c) {
				deferred.resolve(a, b, c);
            })
			.fail(function (a, b, c) {
                deferred.reject(a, b, c);
            })
            
            return deferred.promise();
        },
        getFollowupDefaults: function () {
            var deferred = $.Deferred();
            
			app.api.call({
				request: 'concierge/cs_followup_defaults',
				type: 'GET',
                dataType: 'JSON',
				onComplete: function (a, b, c) {
                    deferred.resolve(a, b, c);
                },
                onError: function (a, b, c) {
                    deferred.reject(a, b, c);
                }
			});
            
            return deferred.promise();
        },
        sendFollowupRequest: function (phone_number, email) {
            var deferred = $.Deferred();
            var data = {
                phone_number: phone_number,
                email: email
            };
			app.api.call({
				request: 'concierge/request_cs_followup',
				type: 'POST',
                data: data,
                dataType: 'JSON',
				onComplete: function (a, b, c) {
                    deferred.resolve(a, b, c);
                },
                onError: function (a, b, c) {
                    deferred.reject(a, b, c);
                }
			});
            
            return deferred.promise();
        },
		login: function (email, password) {
			return new app.Request({
				url: 'auth/login',
				type: 'POST',
				data: {
					email: email,
					password: password
				}
            });
        },
		getPreferences: function () {
			return new app.Request({
				url: 'user/preferences',
				type: 'GET',
				mock: true
            });
        },
		getPreferencesCache: function (data) {
            var deferred = $.Deferred();
			app.api.getPreferences().then(function (data) {
				//-- Update user cache
				var user = app.getUser(app.getData('email'));
				user.set('preferences', data);
				
				deferred.resolve(data);
            }).fail(function () {
                deferred.reject();
            });
			
            return deferred.promise();
        },
		setPreference: function (identifier, value) {
			return new app.Request({
				url: 'user/preferences/' + encodeURIComponent(identifier),
				type: 'POST',
				contentType: 'application/json',
				data: JSON.stringify({
					value: value ? "true" : "false"
				})
            });
        },
		getSocialContent: function (lab_result_id, service) {
			if (service == null) {
				service = 'com.twitter.tweet';
            }
			
			return new app.Request({
				url: 'results/' + encodeURIComponent(lab_result_id) + '/social_content/' + encodeURIComponent(service),
            });
        },
		getReferrals: function () {
			return new app.Request({
				url: 'referrals'
            });
        },
		getReferralsCache: function (data) {
            var deferred = $.Deferred();
			app.api.getReferrals().then(function (data) {
				//-- Update user cache
				var user = app.getUser(app.getData('email'));
				user.set('referrals', data);
				
				deferred.resolve(data);
            }).fail(function () {
                deferred.reject();
            });
			
            return deferred.promise();
        },
		getReferral: function (id) {
			return new app.Request({
				url: 'referrals/' + encodeURIComponent(id)
            });
        },
		submitReferral: function (id, outcome, recipients) {
			return new app.Request({
				url: 'referrals/' + encodeURIComponent(id) + '/submit',
				data: {
					"outcome": outcome,
					"recipients": recipients
				},
				type: 'POST'
			});
        }
	}
})(window);