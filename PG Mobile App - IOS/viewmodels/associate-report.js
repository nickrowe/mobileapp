(function (global) {
    var AssociateReportModel,
        app = global.app = global.app || {},
		defaults = {
            //activation_code: '',
            accession_no: ''
            //,date_of_birth: ''
             //activation_code: 'PGRSH-CAAXC',
             //accession_no: 'D8416520',
             //date_of_birth: '04/15/1967'
        },
    	current_dissassociate_activation_code;
    
    //kinda hacky
    if(typeof app.pages == 'undefined'){
		app.api.resetSyncData();
    }

    AssociateReportModel = function () {
        return kendo.observable({
			removeReport: function (e) {
				if ($(e.target).hasClass('remove')) {
					var li = $(e.target).closest('li');
					var activation_code = li.data('activation_code');
                    current_dissassociate_activation_code = activation_code;
                    app.showConfirmDissassociatReport();
                }
            },
            fields: defaults,
            pages: app.pages
        });
    };
	
	function submitAssociateReportForm(){
        //console.log('submitAssociateReportForm');
		var data = app.associateReportService.viewModel.get('fields').toJSON();
		var errors = [];
		
		if (!errors.length) {
			app.application.showLoading();
            
            if (typeof data.date_of_birth === 'string') {
                if (data.date_of_birth.length == 0) {
                    data.date_of_birth = $('#associate_report .associate-report-dob').val();
                }
            }
			
            data.date_of_birth = (new moment(data.date_of_birth)).format('YYYY-MM-DD');
            
            var user = app.getUser(app.getData('email'));
            user.set('date_of_birth', data.date_of_birth);
            data.accession_no = data.accession_no.toUpperCase();
            }
			
			app.api.addReport(data.accession_no, data.date_of_birth)
			.done(function (response_data, textStatus, request) {
                app.application.hideLoading();
				var errors = [];
				if(typeof response_data.errors != 'undefined' && Object.keys(response_data.errors).length){
					for(var field in response_data.errors){
						errors.push(response_data.errors[field]);
					}
				}
				else if((response_data.status !== true && response_data.status !== 'true') || request.status != 200){
					//according to docs, request.status will be 400 or 500 on failure
					errors.push('There was an error associating the report.');
				}
				
				if(errors.length){
                    //console.log(errors);
					//app.showError(errors.join('<br />'));
                    app.application.navigate('views/welcome/step-3.html?accession_no=' + data.accession_no.toUpperCase());
				}else{
					var sync_promise = app.api.sync();
					var modal_promise = app.showSuccess('The report has been associated.', 'Report Associated').done(function(){
                        if(sync_promise.state() == 'pending'){
                        	app.application.showLoading();
                        }
                    });
					
					$.when(modal_promise, sync_promise).done(function(){
                        app.application.hideLoading();
						if ($('#categories_results_list_accordion').getKendoPanelBar()) {
							$('#categories_results_list_accordion').getKendoPanelBar().destroy();
			            }
						app.categories_results_list.buildAccordion();
					});
					
					//reset to defaults
					for(var key in defaults){
						app.associateReportService.viewModel.set('fields.' + key, defaults[key]);
					}
				}
				
            })
			.fail(function (qXHR, textStatus, errorThrown) {
				//A0001599
				var checked, exists;
				if (qXHR.status === 400) {
					checked = new app.Request({url: 'order'}).then(function (orders) {
						orders.forEach(function (order, i) {
							if (order.accession.number === data.accession_no) {
								exists = true;
                            }
                        });
                    });
                }
				
				$.when(checked).always(function () {
					
					app.api.sync().always(function () {
						app.application.hideLoading();
						if (navigator.network.connection.type == Connection.NONE) {
							app.showError(app.messages.offlineReporting, 'Unable to Add Report');
		                } else {
							if (exists) {
								app.showSuccess('The report has been associated.', 'Report Associated');
								if ($('#categories_results_list_accordion').getKendoPanelBar()) {
									$('#categories_results_list_accordion').getKendoPanelBar().destroy();
					            }
								app.categories_results_list.buildAccordion();
                            } else {
					            var data = app.associateReportService.viewModel.get('fields').toJSON();
			                    app.application.navigate('views/welcome/step-3.html?accession_no=' + data.accession_no.toUpperCase());	
                            }
		                }
	                });
                });
            });
			
			//app.api.call({
			//	request: 'user/associate',
			//	data: data,
			//	type: 'POST',
			//	onComplete: handleResponse,
            //    onError: function (request, textStatus, errorThrown) {
            //        if (navigator.network.connection.type == Connection.NONE) {
            //        	app.showError('Looks like your device is offline. Please try again when you have a network connection.');
            //        } else {
			//            var data = app.associateReportService.viewModel.get('fields').toJSON();
	        //            app.application.navigate('views/welcome/step-3.html?accession_no=' + data.accession_no.toUpperCase());
            //        }
            //    }
			//});
		//} else {
		//	app.showError(errors.join('<br />'));
		//}
	}

    app.associateReportService = {
        init: function (e) {
			var content = e.view.content;
            var datepicker = content.find('.datepicker');
			
			var form = content.find('#associate-report-form');
			form.submit(function (evt) {
				evt.preventDefault();
                $('#associate-report-form input').blur();
                if(app.isInDemoAccount()){
                    app.showError('Associating a report is disabled for demo account: '+app.loginService.viewModel.get('demo_email'));
                    return;
                }
                //-- Start For Andriod
                //app.associateReportService.viewModel.set('activation_code', $('[name=activation_code]').val());
                app.associateReportService.viewModel.set('accession_no', $('#associate_report [name=accession_no]').val());
				//-- End For Android
                submitAssociateReportForm();
			});
			
			//e.view.footer.find('#associate-report-btn').kendoTouch({
			//	tap: function(){
			//		form.submit();
			//	}
			//});
			
            if (typeof datepicker !== 'undefined' && typeof datepicker.datepicker !== 'undefined') {
				datepicker.datepicker(app.date_of_birth_options);
            }
			
            //datepicker.mousedown(function(e){
            //    e.preventDefault();
            //});
            
            //input mask only works good on iOS. Android does not fire keydown/keyup events so the mask plugin has a hack to detect it. No other mask plugins work well either.
            //if(typeof device !== 'undefined' && device.platform === 'iOS'){
            //    content.find('[name=activation_code]').mask('*****-*****');
            //    content.find('[name=accession_no]').mask('********');
            //}
            
            //function bindRemoveClick(){
            //    content.find('.remove').off('click').on('click', function(){
			//		//console.log('clicked remove');
            //        if(app.isInDemoAccount()){
            //            app.showError('Dissassociating a report is disabled for demo account: '+app.loginService.viewModel.get('demo_email'));
            //            return;
            //        }
                    
            //        current_dissassociate_activation_code = $(this).closest('li').data('activation_code')
                    
            //        app.showConfirmDissassociatReport();
            //    });
            //}
            //$(document).on('app_data_set', bindRemoveClick);
            
            ////the data might already be present, so attempt to bind right away
            //bindRemoveClick();
            
            content.find('.terms-link').kendoTouch({
				tap: function(e){
                    e.event.preventDefault();
            		app.showEULA(true);
                }
            });
			
			/*e.view.content.find('[class=datepicker]').kendoDatePicker({
				min: new Date(1865, 7, 20),
				max: new Date((new Date()).getFullYear() - 7, 11, 31),
				start: 'century',
				footer: false,
				change: function(){
					app.associateReportService.viewModel.set('date_of_birth', kendo.toString(this.value(), 'd'));
				}
			});*/
            
            function makeUppercase(input){
                //always make it capitalized on Android because of keyboard problems.
                if(device.platform === 'Android' || input.val().length){
                	input.css('text-transform', 'uppercase');
                }else{
                    input.css('text-transform', 'none');
                }
            }
            
            //auto capitalize
            //content.find('.capitalize').on('keyup change blur keydown focus', function(){
            content.find('.capitalize').on('keyup', function(){
                makeUppercase($(this));
            });
            //app.associateReportService.viewModel.bind('change', function(){
            //    content.find('.capitalize').each(function(){
            //        makeUppercase($(this));
            //    })
            //});
            content.find('.capitalize').each(function(){
                makeUppercase($(this));
            })
            
            content.find('[name=accession_no]').on('keyup', function (e) {
                if (e.keyCode === 13) {
                    if (app.pages.online) {
                        app.application.showLoading();
                        app.pages.saveAssociateReport();
                    }
                }
            });
            
            //FastClick.attach(e.view.content[0]);
        },
        disassociateReport: function(e){
            app.closeModal(e);
            
            var api_options = {
    			request: 'user/disassociate',
    			data: {
                    activation_code: current_dissassociate_activation_code
                },
    			type: 'POST',
    			onComplete: function(response_data, status_text, request){
                    var sync_promise = app.api.sync();
                    var modal_promise = app.showSuccess('The report has been disassociated.').done(function(){
                        if(sync_promise.state() == 'pending'){
                        	app.application.showLoading();
                        }
                    });
					
					$.when(modal_promise, sync_promise).done(function(){
                        app.application.hideLoading();
						if ($('#categories_results_list_accordion').getKendoPanelBar()) {
							$('#categories_results_list_accordion').getKendoPanelBar().destroy();
			            }
						app.categories_results_list.buildAccordion();
					});
    			},
                onError: function(){
                    if (navigator.network.connection.type == Connection.NONE) {
                    	app.showError('Looks like your device is offline. Please try again when you have a network connection.');
                    } else {
            			app.showError('There was a problem disassociating this report.');
                    }
                }
    		};
            
    		app.api.call(api_options);
        },
		show: function (e) {
			app.layout.createMenu(e.view.header);
        },
		beforeShow: function (e) {
			if (!app.isLoggedIn()) {
				e.preventDefault();
				app.application.navigate('views/account/login.html');
			}
            
            //app.associateReportService.viewModel.set('fields.activation_code', '');
            app.associateReportService.viewModel.set('fields.accession_no', '');
            
            var user = app.getUser(app.getData('email'));
            var date_of_birth = user.get('date_of_birth');
            if (date_of_birth != null) {
                app.associateReportService.viewModel.set('fields.date_of_birth', date_of_birth);
            } else {
                app.associateReportService.viewModel.set('fields.date_of_birth', '');
            }
		},
        viewModel: new AssociateReportModel()
    };
})(window);