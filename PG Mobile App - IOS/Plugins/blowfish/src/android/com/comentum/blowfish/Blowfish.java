package com.comentum.blowfish;

import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CallbackContext;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import android.util.Log;
import android.util.Base64;

/**
 * This class echoes a string called from JavaScript.
 */
public class Blowfish extends CordovaPlugin {

    @Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
        if (action.equals("echo")) {
            String message = args.getString(0);
            this.echo(message, callbackContext);
            return true;
        }
        else if (action.equals("encode")) {
            String message = args.getString(1);
            this.encode(message, callbackContext);
            return true;
        }
        else if (action.equals("decode")) {
            String message = args.getString(1);
            this.decode(message, callbackContext);
            return true;
        }
        return false;
    }

    private void echo(String message, CallbackContext callbackContext) {
        if (message != null && message.length() > 0) {
            callbackContext.success(message);
        } else {
            callbackContext.error("Expected one non-empty string argument.");
        }
    }

    final protected static char[] hexArray = "0123456789ABCDEF".toCharArray();
    public static String bytesToHex(byte[] bytes) {
        char[] hexChars = new char[bytes.length * 2];
        for ( int j = 0; j < bytes.length; j++ ) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }
        return new String(hexChars);
    }

    private void encode(String data, CallbackContext callbackContext) {
        BlowfishECB b = new BlowfishECB();
        byte[] key = b.getKey(cordova.getActivity());
        b.initialize(key, 0, 8);
        int strlen = data.length();
        byte[] output = b.encode(data);
        String str = Base64.encodeToString(output, Base64.DEFAULT);

        // Log.i("blowfish.data", data);
        // Log.i("blowfish.strlen", Integer.toString(strlen));
        // Log.i("blowfish.encoded-hex", bytesToHex(output));
        // Log.i("blowfish.outlen", Integer.toString(str.length()));
        // Log.i("blowfish.encoded-string", str);

        callbackContext.success(str);
    }

    private void decode(String data, CallbackContext callbackContext) {
        BlowfishECB b = new BlowfishECB();
        byte[] key = b.getKey(cordova.getActivity());
        b.initialize(key, 0, 8);

        byte[] bytes = Base64.decode(data, Base64.DEFAULT);
        String str = b.decode(bytes);

        callbackContext.success(str);
    }
}