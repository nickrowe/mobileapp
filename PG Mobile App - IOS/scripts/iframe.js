function fixHeight(){
	var height = $('#page_content').height();
	
	wrapper.height(height);
	wrapper.find('iframe').height(height);
}

function setStyles(styles){
    $('#page_styles').html('<style>'+styles+'</style>');
}

function setContent(view){
	$('#page_content').html(view);
	
	fixHeight();
    
    //just to make sure
    setTimeout(function(){
        fixHeight();
    }, 50);
    setTimeout(function(){
        fixHeight();
    }, 500);
}