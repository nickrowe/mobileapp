//
//  CDVSpinnerDialog.m
//
//  Created by Domonkos Pál on 2014.01.27..
//
//

#import "CDVSpinnerDialog.h"

@interface CDVSpinnerDialog () {
    UIActivityIndicatorView *indicator;
}

@property (nonatomic, retain) UILabel *label;
@property (nonatomic, retain) UIActivityIndicatorView *indicator;
@property (nonatomic, retain) UIView *overlay;

@end

@implementation CDVSpinnerDialog

@synthesize label = _label;
@synthesize indicator = _indicator;
@synthesize overlay = _overlay;

-(CGRect)rectForView {
    if (UIInterfaceOrientationIsLandscape([UIApplication sharedApplication].statusBarOrientation)) {
        return CGRectMake( 0.0f, 0.0f, [[UIScreen mainScreen]bounds].size.height, [UIScreen mainScreen].bounds.size.width);
    }
    return CGRectMake( 0.0f, 0.0f, [[UIScreen mainScreen]bounds].size.width, [UIScreen mainScreen].bounds.size.height);
}


-(UIView *)overlay {
    if (!_overlay) {
        _overlay = [[UIView alloc] initWithFrame:self.rectForView];
        _overlay.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.25];
        _indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        _indicator.center = _overlay.center;

        CGFloat labelX = _indicator.bounds.size.width + 2;

        _label = [[UILabel alloc] initWithFrame:CGRectMake(labelX, 0.0f, _overlay.bounds.size.width - (labelX + 2), _overlay.frame.size.height)];
        _label.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        _label.font = [UIFont boldSystemFontOfSize:12.0f];
        _label.numberOfLines = 1;

        _label.backgroundColor = [UIColor clearColor];
        _label.textColor = [UIColor whiteColor];
        _label.text = @"Loading..";

        [_overlay addSubview:_label];

        [_indicator startAnimating];
        [_overlay addSubview:_indicator];
    }
    return _overlay;
}


- (void) show:(CDVInvokedUrlCommand*)command {
    
    
    UIViewController *rootViewController = [[[[UIApplication sharedApplication] delegate] window] rootViewController];
    
    [rootViewController.view addSubview:self.overlay];
    
    
}


- (void) hide:(CDVInvokedUrlCommand*)command {
    if (_overlay) {
        [self.indicator stopAnimating];
        [self.indicator removeFromSuperview];
        [self.overlay removeFromSuperview];
        _label = nil;
        _indicator = nil;
        _overlay = nil;
    }
}

#pragma mark - PRIVATE METHODS


@end


