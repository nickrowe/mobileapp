(function (global) {
    var app = global.app = global.app || {},
    	content,
    	scroller,
		panel_bar,
		can_activate = null;		//the one that was last automatically opened
    function buildAccordion(){
		if ($('#categories_results_list_accordion').getKendoPanelBar()) {
			//$('#categories_results_list_accordion').getKendoPanelBar().destroy();
			return;	
        }
        var collapse_duration = 200;
        
        var template = kendo.template($('#categories_results_list_template').html());
        var result = template({});
        
        $('#categories_results_list_accordion_wrapper').html(result);
        
        function getMaxScroll(content) {
            var height = $('#categories_results_list_accordion').height();
            var height2 = content.height();

            if (height < height2) {
                //--  Don't overscroll!
                return 0;
            } else {
                return height - content.height(); //height ? height - content.height() : 0;
            }
        }
        
        kendo.bind($('#categories_results_list_accordion'), app.pages.data);
        
		panel_bar = $('#categories_results_list_accordion').kendoPanelBar({
			expandMode: 'multiple',
            animation: {
                collapse: {
                    duration: collapse_duration
                }
            },
            collapse: function() {
                //wait for collapse to finish
                setTimeout(function () {
                    global.current_category_id = null;
                    //make sure there's no white space at bottom
                    var max_scroll = getMaxScroll(content);
                    if(scroller.scrollTop > max_scroll){
                        scroller.animatedScrollTo(0, -1 * max_scroll + 3);		//not sure why 3 is needed to make it line up right
                    }
                }, collapse_duration + 300);
            },
            expand: function (evt) {
				if (!can_activate) {
					evt.preventDefault();
					return;
                }
                setTimeout(function () {
                    var ul = $(evt.item).closest('ul');
                    global.current_category_id = $(evt.item).data('category_id');
                    var lis = ul.find('> li');
                    var max_scroll = getMaxScroll(content);
                    var offsetTop = Math.min(evt.item.offsetTop, max_scroll);
                    scroller.animatedScrollTo(0, -1 * offsetTop);
                }, collapse_duration + 300);
            }
		}).data('kendoPanelBar');
    }

    app.categories_results_list = {
		lastScroll: 0,
		buildAccordion: buildAccordion,
        init: function (e) {
            content = e.view.content;
            scroller = e.view.scroller;
			scroller.bind("scroll", function(e) {
				scroller.element.addClass('scrolling');
			});
			content.bind('touchend', function (e) {
				scroller.element.removeClass('scrolling');
            });
            
            //rebuild accordion every time it changes. (Can't use normal binding to reflect change because kendoPanelBar removes binding)
            app.pages.bind('change', function(e){
                //only need to rebuild accordion if the categories_accordions array changes
                if(e.field == 'data' || e.field == 'data.categories_accordions'){
                	buildAccordion();
                }
            });
            
            //buildAccordion();
            
            //if (kendo.mobile.application.os.android) {
                //$('.scroller-wrapper').kendoMobileScroller({
                //    useNative: false
                //});
            //}
            //FastClick.attach(content[0]);
			var touchPoint = {
				x: 0,
				y: 0
            };
			
			var userEvents = new kendo.UserEvents($('#categories_results_list_accordion_wrapper'), {
                press: function(e) {
                    touchPoint = {
                        y: e.touch.y.client,
                        x: e.touch.x.client,
						time: new Date().getTime()
                    };
				},
                release: function(e) {
					console.log(touchPoint);
					var deltaPoint = {
                        y: e.touch.y.client,
                        x: e.touch.x.client,
						time: new Date().getTime()
                    };
					
                    if (Math.abs(touchPoint.y - deltaPoint.y) < 10 && Math.abs(touchPoint.x - deltaPoint.x) < 10) {
	                    var li = $(e.event.target).closest('li.lifestyle-results-list-li-li');
						var share = $(e.event.target).closest('.share-icon');
						if (li.length > 0 && share.length === 0) {
							var result_id = li.data('result-id');

		                	var text = li.closest('ul').prev('.k-link.k-header').find('.name').text();
		                	if (text.match(/product/i) !== null) {
		                		text = 'Clinical Genetics Tests';
		                	}
		                	var href = '#views/result.html?result_id=' + encodeURIComponent(result_id) + '&title=' + encodeURIComponent(text);
							setTimeout(function () {
								document.activeElement && document.activeElement.blur();
								document.activeElement = null;
								app.categories_results_list.lastScroll = $('#categories_results_list').getKendoMobileView().scroller.scrollElement.scrollTop();
								app.application.navigate(href);
                            }, 50);
						}
						else if (share.length > 0) {
							app.result.share(e.event);
                        }
                    }
                }
            });
			
			//$('#categories_results_list .km-native-scroller').bind('scroll', function (e) {
			//	touchPoint = {
			//		x: 0,
			//		y: 0
            //    };
            //});
        },
        beforeShow: function (e) {
			can_activate = false;
			if(!app.isLoggedIn()){
				e.preventDefault();
				app.application.navigate('views/account/login.html');
			}
        },
        show: function (e) {
			app.layout.createMenu(e.view.header);
		},
        afterShow: function (e) {
			can_activate = true;
            var category_id = app.GET('category_id') || e.view.params.category_id;       //e.view.params.category_id
			if (global.current_category_id == null) {
    			var selected_li = $('#categories_results_list_accordion li[data-category_id='+category_id+']');
    			
    			
                if(selected_li.length) {
                    panel_bar.collapse($('#categories_results_list_accordion li'), false);
    				panel_bar.expand(selected_li, false);
					can_activate = false;
                    
                    //e.view.scroller.scrollTo(0, -(selected_li.offset().top - selected_li.height() + 5));
                    var max_scroll = Math.max(0, $('#categories_results_list_accordion').height() - content.height());
                    var scroll_y = Math.min(selected_li.offset().top - e.view.header.height(), max_scroll);
                    
    				scroller.scrollTo(0, 0);
                    scroller.animatedScrollTo(0, -scroll_y);
            		
           		 global.current_category_id = category_id;
					setTimeout(function () {
						can_activate = true;
                    }, 200);
    			}
            } else {
				console.log('lastscroll: ' + app.categories_results_list.lastScroll);
				scroller.scrollTo(0, app.categories_results_list.lastScroll);
            }
        }
    };
})(window);