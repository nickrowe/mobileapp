(function (global) {
    var app = global.app = global.app || {},
    	view,
    	iframe_wrapper,
    	iframe_deferred = $.Deferred();
	
    app.result = {
        init: function(e){
            /*iframe_wrapper = $('#result-iframe-wrapper');
            var iframe = iframe_wrapper.find('iframe');
            
            iframe_wrapper.find('.iframe-mask').on('touchstart touchend click dblclick keyup keydown keypress', function(e){
                app.iframeClickThrough(e, iframe, view);
            });
            
            iframe.load(iframe_deferred.resolve);*/
            $('#result-wrapper').on('click', 'a[href]', function (evt) {
                evt.preventDefault();
                evt.stopPropagation();
                window.open($(this).attr('href'), '_system', 'location=yes&enableViewportScale=yes');
            });
        },
		beforeShow: function (e) {
			if(!app.isLoggedIn()){
				e.preventDefault();
				app.application.navigate('views/account/login.html');
			}
            
            //the data may not be loaded from the local DB yet
            if(typeof app.data == 'undefined'){
                app.application.navigate('#lifestyle-view');
            }
		},
		show: function(e){
            view = e.view;
            
            var result_id = view.params.result_id;
            var result = app.data.results.user.results[result_id];

			if (result.social != null) {
				console.log(result.social);
				if (!result.social.enabled) {
					$(e.view.element).find('.sharable-footer').hide();
                } else {
					$(e.view.element).find('.sharable-footer').data('result-id', result_id).show();
                }
            } else {
				$(e.view.element).find('.sharable-footer').hide();
            }
			
			if (typeof result === 'undefined') {
				app.application.navigate('#views/reports.html');
				e.preventDefault();
				return;
            }
	        e.view.header.find('[data-role=view-title]').text('Result');
            //$('#result-wrapper').html(
			//	'<div style="text-align: center; margin-top: 2em">' +
            //    '<div class="loading" style="margin: 0 auto;">' +
            //    '    <div class="outer"></div>' +
            //    '    <div class="inner"></div>' +
            //    '</div>' +
            //    'Loading...' +
			//	'</div>'
            //);
			$('#result-wrapper').html('');
	        view.scroller.scrollTo(0, 0);
			app.layout.createMenu(view.header);
            
			setTimeout(function () {
				var documentFragment = $(document.createDocumentFragment());
				documentFragment.append('<style>' + app.data.media.css["phenotype-result"] + '</style>');
				documentFragment.append(result.view);
				
	            $('#result-wrapper').append(documentFragment);
            }, 50);
		},
        afterShow: function(e){
			
        },
		share: function (e) {
			e.preventDefault();
			
			var result_id = $(e.target).closest('.sharable-footer, .sharable').data('result-id');
            var result = app.data.results.user.results[result_id];
			
			var sharingRequest = app.api.getSocialContent(result_id);

			$('#warning-modal').data('category', result.social.category);
			$('#warning-modal input').attr('checked', false);
			
			var shareFn = function () {
				sharingRequest.then(function (data) {
					if (data.text != null && data.subject != null) {
						window.plugins.socialsharing.share(data.text, data.subject);
	                }
					else if (data.text != null) {
						window.plugins.socialsharing.share(data.text);
	                }
					else {
						app.showNotice(app.messages.offlineGeneral).then(function (e) {
							//...
		                });
	                }
                })
				.fail(function (req, data, c) {
					if (data === 'error' && req.status === 0) {
						//-- They were online and then went offline, but its not confirmed...
						app.showNotice(app.messages.offlineExplict).then(function (e) {
							//...
		                });
                    }
					else if (navigator.network.connection.type == Connection.NONE) {
						app.showNotice(app.messages.offlineExplict).then(function (e) {
							//...
		                });
	                }
	            });
            };
			
			var user = app.getUser(app.getData('email'));
			var canShowWarning = null
			var title = 'Notice';
			var notice = app.messages.shareNotice;
			if (result.social.category === 'low-risk') {
				canShowWarning = user.getPreference('com.pathway.marketing.social.warnings.low-risk-phenotype');
				$('#warning-modal').addClass('low-risk');
				$('#warning-modal').removeClass('high-risk');
            }
			else if (result.social.category === 'high-risk') {
				canShowWarning = user.getPreference('com.pathway.marketing.social.warnings.high-risk-phenotype');
				$('#warning-modal').addClass('high-risk');
				$('#warning-modal').removeClass('low-risk');
				title = 'Caution';
				notice = app.messages.shareHighRisk;
            }
			if (canShowWarning == null || canShowWarning === true) {
				app.showWarning(notice, title).then(function (okay) {
					if (okay) {
						shareFn();
                    }
                });
            } else {
				shareFn();
            }
			
			document.activeElement && document.activeElement.blur();
			document.activeElement = null;
			
        }
    };
})(window);