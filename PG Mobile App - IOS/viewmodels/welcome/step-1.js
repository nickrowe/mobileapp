(function (global) {
    var Model,
    	service,
        app = global.app = global.app || {};

    Model = function () {
        return kendo.observable({
            goToCategories: function () {
                app.setData('force_no_reports', 1);
                app.application.navigate('#lifestyle-view');
            },
            goToStep2: function () {
                app.application.navigate('#views/welcome/step-2.html');
            }
    	});
    };

    service = {
        waiting: true,
		beforeShow: function(e) {
            //TODO if they are already have logged in / registered before...
            var loggedin = app.isLoggedIn(function () {
                waiting = false;
                if (app.pages.get('data.reports.length') > 0) {
    				app.application.navigate('#lifestyle-view');
                } else {
    				app.application.navigate('views/welcome/step-1.html?rand=' + Math.random());
                }
            });
			if (!loggedin) {
			    e.preventDefault();
				app.application.navigate('views/account/login.html');
			}
		},
		init: function(e) {
		},
        show: function (e) {
            var viewModel = service.viewModel;
        },
        afterShow: function (e) {
            app.layout.fillCenter(e.view.element);
            navigator.splashscreen.hide();
        },
        viewModel: new Model()
    };
    app.welcomeStep1Service = service;
})(window);