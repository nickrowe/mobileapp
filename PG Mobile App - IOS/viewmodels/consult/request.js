(function (global) {
    var app = global.app = global.app || {},
		Model,
		service;
	
	Model = function () {
        return kendo.observable({
			referral: {
				questions: []
            },
			evaluateQuestions: function (evt) {
				evt.preventDefault();
				var referral = service.viewModel.get('referral').toJSON();
				var questions = service.viewModel.get('referral.questions').toJSON();
				var not_answered = questions.reduce(function (result, question, i) {
					return (question.answer !== '1' && question.answer !== '0') ? false : result;
                }, true);
				
				if (!not_answered) {
					app.showError('To continue, please provide responses to each question.', 'Unanswered Questions');
					return;
                }
				
				var insuranceQuestion = null;
				if (service.viewModel.get('referral.requires_insurance')) {
					insuranceQuestion = questions.pop();
					
                }
				
				var eligible = questions.reduce(function (result, question, i) {
					return (question.answer === '1') ? true : result;
                }, false);
				
				
				console.log(not_answered);
				
				if (eligible) {
					if (service.viewModel.get('referral.requires_insurance')) {
						if (insuranceQuestion.answer === '1') {
							
                        } else {
							eligible = false;
                        }
	                } else {
						
                    }
                }
				
				if (eligible) {
					app.application.navigate('#views/consult/eligible.html?report_id=' + encodeURIComponent(referral.report_id) + '&referral_id=' + encodeURIComponent(referral.id));
                } else {
					app.application.navigate('#views/consult/not-eligible.html?report_id=' + encodeURIComponent(referral.report_id) + '&referral_id=' + encodeURIComponent(referral.id));
                }
            }
        });
    };

    service = {
        init: function (e) {
			FastClick.attach(e.view.content[0]);
        },
		beforeShow: function (e) {
		},
        show: function (e) {
			app.layout.createMenu(e.view.header);
			
			e.view.scroller.reset();
			
			var report_id = e.view.params.report_id;
			var user = app.getUser(app.getData('email'));
			referrals = user.get('referrals');
			var referral = referrals.reduce(function (result, referral, i) {
				if(referral.report_id == report_id) {
					return referral;
                } else {
					return result;
                }
            }, null);
			
			var skipMedicalQuestion = false;
			
			//if (skipMedicalQuestion === false) {
			//	for (var key in app.data.reports) {
			//		if (app.data.reports[key].id == report_id) {
			//			var report = app.data.reports[key];
			//			var label = report.name.toLocaleLowerCase();
			//			if (label.indexOf('pathway') > -1 && label.indexOf('fit') > -1) {
			//				skipMedicalQuestion = true;
	        //            }
            //        }
            //    }
            //}
			
			if (referral != null) {
				referral.getReferrralLabelStyle = function () {
					var referral = service.viewModel.get('referral');
					return 'color: ' + referral.colors.colors.text
						+ '; background: ' + referral.colors.colors.background;
	            };
				if (typeof referral.label !== 'undefined' && skipMedicalQuestion === false) {
					if (!referral.requires_insurance) {
						skipMedicalQuestion = true;
                    }
                }
				
				if (skipMedicalQuestion === false) {
					referral.questions.push({
						id: 'insurance',
						text: 'Do you have medical insurance?'
	                });
                }
				service.viewModel.set('referral', referral);
            }
        },
		afterShow: function (e) {
        },
		hide: function (e) {
        },
		viewModel: new Model()
    };
	
	app.consultRequestService = service;
})(window);