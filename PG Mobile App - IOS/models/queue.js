(function (root) {
	var SAVE_PREFS = 0;
	var SAVE_CONSULT = 1;
	
	function Queue() {
		this.items = [];
		this.interval = null;
		this.processing = false;
		this.processingMax; // TODO: how many items to process at a time.
		this.consts = {
			savePrefs: SAVE_PREFS,
			saveConsult: SAVE_CONSULT
        };
		var that = this;
		this.interval = setInterval(function () {
			that.process();
        }, 5000);
    };
	Queue.prototype.addItemFromParams = function (type, url, data, options) {
		return this.addItem(new QueueItem(type, url, data, options));
    };
	Queue.prototype.addItem = function (item /* QueueItem */) {
		var that = this;
		var added = false;
		this.items.forEach(function (existingItem, i) {
			if (item.type === SAVE_PREFS && existingItem.type === item.type && existingItem.locked === false) {
				// Replace the old instance, but only if its not being processed.
				that[i] = item;
				added = true;
            }
        });
		if (!added) {
			return this.items.push(item);
        }
		
		if (added) {
			this.toDisk();
        }
		
		return added;
    };
	Queue.prototype.toDisk = function () {
		try {
			localStorage.setItem('queue', JSON.stringify(this.items));
        } catch (e) {
			// TODO: Something went really wrong...do I show an error?
        }
    };
	Queue.prototype.fromDisk = function () {
		try {
			var items = JSON.parse(localStorage.getItem('queue'));
			this.items = items;
        } catch (e) {
			// TODO: Something went really wrong...do I show an error?
        }
    };
	Queue.prototype.clear = function () {
		this.items = [];
		localStorage.removeItem('queue');
		
		//-- TODO what if there is a reqest in transit? Do I cancel it?
    };
	Queue.prototype.process = function () {
		if (this.processing) {
			return false;
        }
		var that = this;
		var item = this.items[0];
		
		if (this.items.length > 0) {
			this.processing = true;
			item.fire().then(function (e) {
				that.items.shift();
	        }).always(function (e) {
				that.processing = false;
	        });
        }
		
		return true;
    }
	
	function QueueItem(type, url, data, options) {
		this.type = type;
		this.url = url;
		this.data = data;
		this.options = options || {};
		this.locked = false;
    }
    QueueItem.prototype.fire = function () {
		this.locked = true;
		var deferred = $.Deferred();
		var that = this;
		
		
		if (navigator.network.connection.type === Connection.NONE) {
			this.locked = false;
			deferred.reject();
        } else {
			var options = $.extend(this.options, {
				url: this.url,
				data: this.data,
				retryMax: 0
	        });
			new app.Request(options).then(function (data) {
				if (data.status) {
					deferred.resolve();
	            } else {
					//-- TODO if there are any queue types that are allowed to fail?
					deferred.reject();
	            }
	        }).fail(function (e) {
				//-- TODO if there are any queue types that are allowed to fail?
				deferred.reject();
	        }).always(function () {
				that.locked = false;
	        });
        }
		
        return deferred.promise();
    }
	
    
    root.app.queue = new Queue();
}(window));