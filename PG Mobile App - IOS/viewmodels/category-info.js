(function (global) {
    var app = global.app = global.app || {},
    	view,
    	iframe_deferred = $.Deferred();
	
    app.category_info = {
        init: function(){
            var iframe = $('#category-info-iframe-wrapper iframe');
            $('#category-info-iframe-wrapper').on('touchstart touchend click dblclick keyup keydown keypress', function(e){
                app.iframeClickThrough(e, iframe, view);
            });
            
            iframe.load(iframe_deferred.resolve);
        },
		beforeShow: function (e) {
			if(!app.isLoggedIn()){
				e.preventDefault();
				app.application.navigate('views/account/login.html');
			}
			
			var category = app.data.categories[app.GET('category_id')];
            
            iframe_deferred.done(function(){
                $('#category-info-iframe-wrapper iframe')[0].contentWindow.setStyles(app.data.media.css["phenotype-result"]);
                $('#category-info-iframe-wrapper iframe')[0].contentWindow.setContent(category.view);
                e.view.scroller.scrollTo(0, 0);
            });
		},
		show: function(e){
            view = e.view;
		}
    };
})(window);