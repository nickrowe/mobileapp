(function (global) {
    var app = global.app = global.app || {};
    
    app.test_local = false;     //use local data for testing
    
    app.resetData = function () {
		app.removeData('remember');
		app.removeData('password');
		app.removeData('data');
		app.removeData('data_date');
		app.removeData('last_activity_date');
        app.removeData('force_no_reports');
    };
    
    app.loadData = function () {
        var deferred = $.Deferred();
        
        var data_date = app.getData('data_date');
        var data_expired = typeof data_date === 'undefined' || data_date < $.now() - (1000 * 60 * 60 * 5);
        
        if (data_expired) {
            app.api.resetSyncData();
            deferred.resolve(false);
            return deferred.promise();
        }
        
        app.getData('data', function (data) {
            if (data) {
                app.api.setAppData(data).then(function () {
                    deferred.resolve(true);
                });
            } else {
                app.api.resetSyncData();
                app.removeData('data_date');
                app.removeData('last_activity_date');
                deferred.resolve(false);
            }
        });
        
        return deferred.promise();
    };

    $.ajaxSetup({
        headers: {
            'X-Pathway-App': app.version
        }
    });

    //-- Custom View Loading Per Platform
    $(document).ajaxSend(function (event, jqxhr, settings) {
        
        // alert('ajax send');
        
        // This is not a perminate solution, but will do for now.
        var ios = typeof kendo.mobile.application !== 'undefined' && typeof kendo.mobile.application.os.ios !== 'undefined',
            android = typeof kendo.mobile.application !== 'undefined' && typeof kendo.mobile.application.os.android !== 'undefined',
            url = settings.url.split('?')[0];

        if (app.routes[url] != null) {
            var route = app.routes[url];
            if (ios && typeof route.ios !== 'undefined') {
                settings.url = route.ios;
            }
            else if (android && typeof route.android !== 'undefined') {
                settings.url = route.android;
            }
        }
    });
    
    $(document).ajaxSuccess(function(event, XMLHttpRequest, ajaxOptions) {     
        
        //var data = $.httpData(XMLHttpRequest, ajaxOptions.dataType, ajaxOptions);
        var is_remote = ajaxOptions.url.indexOf(app.remoteHost) > -1;
        var data = null;
        var that = this;
        if (ajaxOptions.dataType === 'json' && is_remote) {
            try {
                var data = JSON.parse(XMLHttpRequest.responseText);
            } catch (e) {
                app.log(e);
            }
        }

        // console.dir('<-- url:' + ajaxOptions.url + ', resp:' + XMLHttpRequest.responseText);

        if (is_remote && typeof ajaxOptions.dataType !== 'undefined' && ajaxOptions.dataType === 'json') {
            //-- Server Error
            // console.dir(ajaxOptions.url, data !== null, data);
            if (typeof data !== 'object') {
                alert('Sorry there is something wrong with the server');
            }
        }
    });
    
    var max_year = (new Date()).getFullYear() - 7;
    app.date_of_birth_options = {
        // showOn: 'button',
        // buttonImage: 'css/images/calendar.gif',
        minDate: new Date(1865, 7, 20),
        maxDate: new Date(max_year, 11, 31),
        changeMonth: true,
        changeYear: true,
        yearRange: '1865:'+max_year,
        showButtonPanel: true,
        closeText: 'Clear'
    };
    
    //Modal Stuff (could really use some re-factoring!)
    app.closeModal = function(e) {
        app._closeModal(e.sender.element.closest('[data-role=modalview]'), $(e.target).hasClass('ok'));
        var loggedIn = app.isLoggedIn();
        if (loggedIn && $(e.target).closest('#eula-modal').length > 0 && $(e.target).text() !== 'Accept') {
            app.loginService.logout().then(function (e) {
                app.application.navigate('views/account/login.html?rand=' + Math.random());
            });
        }
    }
    app._closeModal = function(modal, okay) {
		if (okay == null) {
			okay = false;
        }
        var modalView = modal.data('kendoMobileModalView');
        modalView.close();
        
        var deferred = modal.data('deferred');
        
        if(deferred){
            deferred.resolve(okay);
        }
    }
    app.modalInit = function(e){
        //add class to modal wrapper so we can style the border
        var class_name = e.sender.element.attr('id')+'-wrapper';
        $(e.sender.wrapper).addClass(class_name).parent().addClass(class_name+'-wrapper');
        
        $(e.sender.wrapper).find('.close').kendoTouch({
            tap: function(e){
                e.event.preventDefault();
                app.closeModal(e);
            }
        });
        
        $(e.sender.element).find('.km-scroll-container').on('click touchstart', 'a[href]', function (evt) {
			if ($(this).attr('href').indexOf('#views/') === -1) {
	            evt.preventDefault();
	            evt.stopPropagation();
            	window.open($(this).attr('href'), '_system', 'location=yes&enableViewportScale=yes');
            }
        });
    }
	
	
    app.modal = function(type, message, title){
        type = typeof type == 'undefined' ? 'notice' : type;
        
        var deferred = $.Deferred();
        
        if(typeof title != 'undefined'){
            $('#'+type+'-modal [data-role=view-title]').html(title);
        }
        
        if(typeof message != 'undefined'){
            $('#'+type+'-modal .content').html(message);
        }
        
        $('#'+type+'-modal').data('deferred', deferred);
        
        //first blur any focused field (because kendo has a problem with the keyboard being up)
        $(':focus').blur();
		
		if ($('#'+type+'-modal').data('kendoMobileModalView')) {
			$('#'+type+'-modal').data('kendoMobileModalView').scroller.reset();
        }
        
        $('#'+type+'-modal').data('kendoMobileModalView').open();
        
        return deferred.promise();
    }
    app.showError = function(error, title){
        return app.modal('error', error, title);
    }
    app.showSuccess = function(success, title){
        return app.modal('success', success, title);
    }
    app.showNotice = function(notice, title){
        return app.modal('notice', notice, title);
    }
    app.showWarning = function(notice, title){
        return app.modal('warning', notice, title);
    }
    app.showEULA = function(closeable){
        closeable = typeof closeable == 'undefined' ? false : closeable;
        
        if(closeable){
            $('#eula-modal').addClass('closeable');
        }else{
            $('#eula-modal').removeClass('closeable');
        }
        
        var modal = app.modal('eula');
        
        
        setTimeout(function () {
            $('#eula-modal').getKendoMobileModalView().scroller.scrollTo(0,0);
        }, 100);
        
        return modal;
    }
    app.showConfirmDissassociatReport = function(){
        return app.modal('confirm-report-disassociate');
    }
    
    
    app.isInDemoAccount = function(){
        return app.getData('email') === app.loginService.viewModel.get('demo_email');
    }
    
    var onInit = function(){
        app.inited = true;
        if(typeof app.pages == 'undefined'){
            app.api.resetSyncData();
        }
        
        $('#eula-modal a[href]').click(function(e){
            e.preventDefault();
            app.externalURL($(this).attr('href'));
        });
		
        //precache result images
        $(document).on('app_data_set', function(){
            if(
                !app.checkNested(navigator, 'network', 'connection', 'type')
                || typeof Connection === 'undefined'
                || navigator.network.connection.type !== Connection.WIFI 
                || !app.checkNested(app, 'data', 'results', 'user', 'results')
            ){
                //return;
            }
            
            var results_string = '';
            for(var results_id in app.data.results.user.results){
                results_string += app.data.results.user.results[results_id].view;
                
                if(app.data.results.user.results[results_id].view.indexOf('img') !== -1){
                    //console.log(app.data.results.user.results[results_id]);
                }
            }
            
            //give it a second. when the app data is set, the whole UI has to reflect the changes in the data so let it do it's thing first. 
            setTimeout(function(){
                //this loads/caches all the images
                $(results_string);
            }, 1000);
        });
        
        //if(typeof navigator !== 'undefined'){
        //    //sometimes the phone didn't have this defined yet
        //    function hideInitialSplashScreen() {
        //        if(app.checkNested(navigator, 'splashscreen', 'hide')){
        //            navigator.splashscreen.hide();
        //        }else{
        //            setTimeout(hideInitialSplashScreen, 100);
        //        }
        //    }
        //    hideInitialSplashScreen();
        //}
    }
    
    app.genericError = function(){
        app.showError('An error has occurred').done(function(){
            try{
                app.application.navigate('#lifestyle-view');
            }catch(e){
                app.application.navigate('views/account/login.html');
            }
        });
    }
    
    //initial = 'views/result.html?result_id=4259633';
    //initial = 'views/survey.html?survey_id=3';
    //initial = 'views/associate-report.html';
    //initial = 'views/icons.html';
    
    function onDeviceReady() {
        try{
            navigator.splashscreen.show();
        }catch(e){}
        
        app.datastoreInit();
        
        var user = app.getUser(app.getData('email'));

        var agreed_version = user.get('agreement_version');
        if (agreed_version == null || agreed_version != app.version) {
            app.resetData();
        }
        
        var initial, done;
        if (app.isLoggedIn()) {
            done = $.Deferred();
			app.loadData()
			.then(function (hasData) {
				if (hasData) {
					$.when(app.api.getReferralsCache(), app.api.getPreferencesCache()).always(function () {
						done.resolve(hasData);
                    });
                } else {
					var failedSync = function () {
						done.resolve(false);
						app.removeData('remember');
						app.removeData('password');
                    }
					
					app.api.sync()
					.then(function () {
						app.loadData()
						.then(function (hasData) {
							$.when(app.api.getReferralsCache(), app.api.getPreferencesCache()).always(function () {
								done.resolve(hasData);
		                    });
                        })
						.fail(failedSync);
                    })
					.fail(failedSync);
                }
            }).fail(function () {
				done.resolve(false);
            });
        }
		
        
        $.when(done).then(function (hasData) {
            var skipWelcome, skipIntro;
            if (hasData) {
                skipWelcome = app.pages.get('data.activations.length') > 0 || app.getData('force_no_reports');
            }
            skipIntro = app.getData('used_before') == 1;
            
            
            if (app.isLoggedIn()) {
                initial = '#lifestyle-view';
            } else {
                initial = 'views/introduction.html';
            }
            
            app.application = new kendo.mobile.Application(document.body, {
                layout: 'login-layout',
                initial: initial,
				// transition: "fade",
                skin: 'ios7',
                init: onInit
            });
            
            //-- Aspect around navigate for debugging
            //var nav = app.application.navigate;
            //app.application.navigate = function (e, t) {
            //    console.log('navigating to "' + e + '"');
            //    console.trace();
            //    this.pane.navigate(e, t);
            //}
            
            app.application.changeLoadingMessage(
                '<div class="loading" style="margin: 0 auto;">' +
                '    <div class="outer"></div>' +
                '    <div class="inner"></div>' +
                '</div>' +
                'Loading...'
            );

            document.addEventListener("online", function () {
                app.pages.set('online', true);
                app.pages.set('offline', false);
            }, false);
            document.addEventListener("offline", function () {
                app.pages.set('online', false);
                app.pages.set('offline', true);
            }, false);
			
			//document.addEventListener("touchmove", function(event){
			//    event.preventDefault();
			//});



            document.addEventListener("backbutton", function(e){
                if (document.location.href.indexOf('#views/reports.html') > -1) {
                    // do nothing
                } else if (
					document.location.href.indexOf('#views/welcome/step-2.html') > -1 &&
					document.location.href.indexOf('registered') > -1
				) {
                    // do nothing
                } else if (document.location.href.indexOf('#lifestyle-view') > -1) {
                    // do nothing
                } else if (document.location.href.indexOf('#views/welcome.html') > -1) {
                    // do nothing
                } else if (document.location.href.indexOf('#views/account/login.html') > -1) {
                    app.application.navigate('#views/introduction.html');
                } else if (
					document.location.href.indexOf('#views/introduction.html') > -1 ||
					document.location.href.indexOf('#views/welcome/step-1.html') > -1
				) {
                    navigator.notification.confirm(
                        'Are you sure you want to exit?', function (buttonIndex) {
                        if (buttonIndex == 1) {
                            navigator.app.exitApp();
                        }
                    }, 'Exit Application');
                } else {
                    navigator.app.backHistory();
                }
            }, false);

            
            var touchPoint = {
				intervalPop: null,
				elem: null,
                x: 0,
                y: 0
            };

            $(document.body).on('touchstart', '.km-back, .k-overflow-wrapper .km-button, .absolute-touch', function (e) {
                touchPoint = {
                    y: e.originalEvent.changedTouches[0].pageY,
                    x: e.originalEvent.changedTouches[0].pageX
                };
				touchPoint.elem = e.target;
                if ($(e.target).hasClass('km-back')) {
					$('input:focus').blur();
                }
				
				
				//var menuButton = $(e.target).closest('.k-overflow-button.km-button');
				//if (menuButton.hasClass('k-overflow-button')) {
                //    e.preventDefault();
                //    $('.k-overflow-wrapper').hide();
                //    app.application.navigate(menuButton.attr('href'));
                //}
            });

            $(document.body).on('touchend', '.km-back, .k-overflow-wrapper .km-button, .km-button, .absolute-touch', function (e) {
                var deltaPoint = {
                    y: e.originalEvent.changedTouches[0].pageY,
                    x: e.originalEvent.changedTouches[0].pageX
                };
				var goBack = $(e.target).closest('.km-back');
				var absoluteTouch = $(e.target).closest('.absolute-touch');
				var button = $(e.target).closest('.km-button');
				var menuButton = $(e.target).closest('.k-overflow-button.km-button');
				
                if (Math.abs(touchPoint.y - deltaPoint.y) < 10 && Math.abs(touchPoint.x - deltaPoint.x) < 10) {
                    if (goBack.hasClass('km-back')) {
                        e.preventDefault();
						if (goBack.hasClass('login-back-btn')) {
							app.application.navigate('#views/introduction.html');
                        } else {
                        	app.application.navigate('#:back');
                        }
                    }
					//else if (absoluteTouch.hasClass('absolute-touch') && absoluteTouch.hasClass('menu-toolbar')) {
                    //    e.preventDefault();
					//	e.stopImmediatePropagation();
					//	e.stopPropagation();
                    //    if (absoluteTouch.getKendoToolBar()) {
					//		console.log(absoluteTouch.getKendoToolBar());
					//		absoluteTouch.getKendoToolBar()._toggle(e);
                    //    }
					//	console.log('menu!');
                    //}
					else if ((absoluteTouch.hasClass('absolute-touch') || button.hasClass('km-button')) && absoluteTouch.attr('href')) {
                        e.preventDefault();
						e.stopPropagation();
                        app.application.navigate(absoluteTouch.attr('href'));
                    }
					else if (menuButton.hasClass('k-overflow-button')) {
	                    e.preventDefault();
	                    $('.k-overflow-wrapper').hide();
	                    app.application.navigate(menuButton.attr('href'));
	                }
					else {
                        if ($(e.target).attr('href')) {
                            e.preventDefault();
							//e.stopImmediatePropagation();
                            app.application.navigate(button.attr('href'));
                            $('.k-overflow-wrapper').hide();
                        }
                    }
                }
                else if (Math.abs(touchPoint.y - deltaPoint.y) < 15 && Math.abs(touchPoint.x - deltaPoint.x) < 20) {
                    if (absoluteTouch.hasClass('absolute-touch') && absoluteTouch.attr('href')) {
                        e.preventDefault();
						e.stopPropagation();
                        app.application.navigate($(e.target).attr('href'));
                    }
                }
            });
            
            app.externalLinks();
        });
        
    }
    
    app.gotoPrivacy = function (event) {
		event.preventDefault();
		event.stopPropagation();
		
        var wrapper = $(event.target).closest('.km-scroll-wrapper');
		//-- Is this native scroll?
		if (wrapper.length === 0) {
        	wrapper = $(event.target).parents('.km-native-scroller');
        }
		
        var header = wrapper.find('.policy-header');
        var pos = header.position();
        var scroller = wrapper.getKendoMobileScroller();
        scroller.reset();
        scroller.scrollTo(0, -1 * pos.top);
    };
    
    app.externalLinks = function () {
        $('#privacy-and-terms').on('click touchstart', 'a[href]', function (evt) {
			var href = $(evt.target).attr('href');
			if (href[0] === '#' || !href) {
				
            } else {
				if (href.indexOf('tel:') > -1) {
					
                } else {
		            evt.preventDefault();
		            evt.stopPropagation();
		            window.open(href, '_system', 'location=yes&enableViewportScale=yes');
                }
            }
			//console.log(evt);
        });
    }
    
    window.onload = function () {
        if (typeof document.ondeviceready !== 'undefined') {
            document.addEventListener("deviceready", onDeviceReady, false);
        } else {
            onDeviceReady();
        }
        
    };
	
	window.invokeReferralTree = function (id) {
		$('#preview-closer').hide();
		var user = app.getUser(app.getData('email'));
		referrals = user.get('referrals');
		var referral = referrals.reduce(function (result, referral, index) {
			return (parseInt(referral.id) === id) ? referral : result;
        }, null);
		
		if (referral !== null) {
			if (referral.report_id) {
				app.application.navigate('#views/consult/request.html?report_id=' + encodeURIComponent(referral.report_id));
            }
        }
    }
})(window);