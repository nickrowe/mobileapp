(function (global) {
    var AccountRegisterModel,
        app = global.app = global.app || {},
		defaults = {
            // email: 'rick@comentum.com',
            // first_name: 'Rick',
            // middle_name: 'C',
            // last_name: 'Comentum',
            // password: 'pathrick',
			// password2: 'pathrick'
            email: '',
            first_name: '',
            last_name: '',
            password: '',
			password2: '',
            date_of_birth: '',
            accession_no: ''
        };

    AccountRegisterModel = function () {
        return kendo.observable(defaults);
    };
	
	function submitRegistrationForm(){
		var data = app.accountRegisterService.viewModel.toJSON();
        //app.setData('accession_no', data.accession_no);
        //app.setData('date_of_birth', data.date_of_birth);
        
		var errors = [];
		
		if(data.password != data.password2){
			errors.push('The passwords you entered do not match.');
		}
		
		if(!errors.length){
			app.application.showLoading();
			
			function complete(response_data, status_text, request){
				app.application.hideLoading();
                
                if (navigator.network.connection.type == Connection.NONE) {
                	app.showError('Looks like your device is offline. Please try again when you have a network connection.');
                	return;
                }
				
				var errors = [];
				
				switch (request.status) {
					default:
					case 400:
					case 500:
						//unsuccessful
						errors.push('There was an error registering. Please try again later.');
					break;
					
					case 401:
						//The user is already logged in
					break;
					
					case 200:
						//success (but there still may be errors
						if(typeof response_data.errors != 'undefined' && Object.keys(response_data.errors).length){
							for(var field in response_data.errors){
								errors.push(response_data.errors[field]);
							}
						}
                        /*if (response_data.status === true) {
                            if (response_data.action === 'alreadyRegisteredViaWebsite') {
                            	errors.push('You have an account through the website.');
                            } else {
                            	errors.push('You have an account through the mobile app.');
                            }
                        }*/
					break;
				}
				
				if(errors.length){
					app.showError(errors.join('<br />'));
				}else{
                    //console.log(response_data);
					app.showSuccess(response_data.display.message, response_data.display.title).done(function(){
                        var user = app.getUser(data.email);
                        user.setMany({
                            first_name: data.first_name,
                            last_name: data.last_name,
                            date_of_birth: data.date_of_birth,
                            accession_no: data.accession_no,
                            agreement_version: app.version
                        });
                        
                        app.loginService.viewModel.login(data.email, data.password, false, data.accession_no, data.date_of_birth, true);
					});
					
					//reset to defaults
					for(var key in defaults){
						app.accountRegisterService.viewModel.set(key, defaults[key]);
					}
				}
			}
			
			app.api.call({
				request: 'user/register',
				data: data,
				type: 'PUT',
				onComplete: complete,
                onError: complete
			});
		}else{
			app.showError(errors.join('<br />'));
		}
	}

    app.accountRegisterService = {
        init: function (e) {
            var content = e.view.content;
			var form = content.find('#register-form');
			form.submit(function(evt){
				evt.preventDefault();
				
                //touchend can trigger a phone number or email in the EULA, so to prevent that...
                setTimeout(function(){
                	app.showEULA(false);
                }, 350);
			});
			
			content.find('form input').bind('keyup', function (e) {
				if (e.keyCode === 13) {
					app.showEULA(false);
                }
            })
			
			content.find('.register').kendoTouch({
				tap: function(){
					form.submit();
				}
			});
            
            content.find('.terms-link').kendoTouch({
				tap: function(){
            		app.showEULA(true);
                }
            });
			
			//console.log();
        },
        acceptEULA: function(e){
            app.closeModal(e);
            var loggedIn = app.isLoggedIn();
            
            if (!loggedIn) {
                submitRegistrationForm();
            } else {
                var user = app.getUser(app.getData('email'));
                user.set('agreement_version', app.version);
				app.application.navigate('#lifestyle-view?rand=' + Math.random());
            }
        },
        show: function (e) {
        },
		afterShow: function (e) {
        },
        viewModel: new AccountRegisterModel()
    };
})(window);