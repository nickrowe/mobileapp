(function () {
	'use strict'
	
	var self = {};
	
	//sets scrollview to fill to the full height of its container.
	//needs to be called after the the scrollview has its content filled in.
	self.adjustScrollViewHeight = function ($scope) {	
		var containerHeight = $scope.find('.km-scroll-wrapper').height();
		var kmPagesHeight = $scope.find('.km-pages').height();
		$scope.find('.km-scroll-container').height(containerHeight);
		$scope.find('.km-scrollview').height(containerHeight);
		$scope.find('.km-scrollview > div').height(containerHeight);
		var pages = $scope.find('.km-pages').remove();
		$scope.find('.km-virtual-page').height(containerHeight);
		
		pages.appendTo($scope.find('.show-pages'));
    };

	window.kendoFix = self;
}())