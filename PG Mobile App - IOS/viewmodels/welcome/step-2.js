(function (global) {
    var Model,
    	service,
        app = global.app = global.app || {};

    Model = function () {
        return kendo.observable({
			height: 0,
            accession_no: '',
            nextStep: function () {
                $('#welcome-step-2-view input').blur();
				
                if (navigator.network.connection.type == Connection.NONE) {
					app.showError(app.messages.offlineReporting).then(function (e) {
						app.application.navigate('#lifestyle-view');
                    });
					return;
                }
				
				app.application.showLoading();
                
                //-- Start For Andriod
                service.viewModel.set('accession_no', $('#welcome-step-2-view [name=accession_no]').val());
				//-- End For Android
                
                var accession_no = service.viewModel.get('accession_no').toUpperCase();
                app.api.addReport(
                    accession_no, 
                    service.viewModel.get('date_of_birth')
                ).done(function (resp) {
					var message = app.messages.reportFound;
					if (resp.status) {
                        app.api.sync().then(function () {
							if ($('#categories_results_list_accordion').getKendoPanelBar()) {
								$('#categories_results_list_accordion').getKendoPanelBar().destroy();
				            }
							app.categories_results_list.buildAccordion();
    						app.application.hideLoading();
    						app.application.navigate('views/welcome/step-7.html?message=' + encodeURIComponent(message));
                        });
                    } else {
					    app.application.navigate('views/welcome/step-3.html?accession_no=' + encodeURIComponent(accession_no));
                    }
                }).fail(function (resp) {
                    if (navigator.network.connection.type == Connection.NONE) {
						app.showError(app.messages.offlineReporting).then(function (e) {
							app.application.navigate('views/categories.html');
                        });
						return;
                    }
					app.application.navigate('views/welcome/step-3.html?accession_no=' + encodeURIComponent(accession_no));
                });
            }
        });
    };

    service = {
        waiting: true,
		beforeShow: function(e) {
		},
        init: function (e) {
			var content = e.view.content;
            
            function makeUppercase(input){
                //always make it capitalized on Android because of keyboard problems.
                if(device.platform === 'Android' || input.val().length){
                	input.css('text-transform', 'uppercase');
                }else{
                    input.css('text-transform', 'none');
                }
            }
            
            //auto capitalize
            content.find('.capitalize').on('keyup', function(){
                makeUppercase($(this));
            });
            content.find('.capitalize').each(function(){
                makeUppercase($(this));
            });
            
            //FastClick.attach(e.view.content[0]);
        },
        show: function (e) {
            var viewModel = service.viewModel;
            
            //-- Auto fill the birthday with what is stored on the device
            var user = app.getUser(app.getData('email'));
            viewModel.set('date_of_birth', user.get('date_of_birth'));
            
            if (!viewModel.get('accession_no.length')) {
                viewModel.set('accession_no', e.view.params.accession_no);
            }
            
            if (e.view.params.registered) {
                e.view.content.find('.step-2-back-btn').css({'visibility': 'hidden'});
            } else {
                e.view.content.find('.step-2-back-btn').css({'visibility': 'visible'});
            }
        },
        afterShow: function (e) {
            app.layout.fillCenter(e.view.element);
        },
        viewModel: new Model()
    };
    app.welcomeStep2Service = service;
})(window);