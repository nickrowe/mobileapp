(function (global) {
    var app = global.app = global.app || {},
        header_menu_height = 500,
		header_shown = false,
		ww = 0,
        menu_speed = 200;
	
	function getDefaultMenuItems () {
			return [
                {
                    'type': 'button',
                    'text': 'Lifestyle',
					'url': '#lifestyle-view',
                    'overflow': 'always'
                },
                {
                    'type': 'button',
                    'text': 'Reports',
					'url': '#views/reports.html',
                    'overflow': 'always'
                },
                {
                    'type': 'button',
                    'text': 'Connect a Report',
					'url': '#views/associate-report.html',
                    'overflow': 'always'
                },
                {
                    'type': 'button',
                    'text': 'Preferences',
					'url': '#views/account/preferences.html',
                    'overflow': 'always'
                },
                {
                    'type': 'button',
                    'text': 'Privacy & Terms',
					'url': '#views/privacy-and-terms.html',
                    'overflow': 'always'
                },
                {
                    'type': 'button',
                    'text': 'Logout',
					'url': '#views/account/logout.html',
                    'overflow': 'always'
                }
			];
        }
	
    app.layout = {
		getDefaultMenuItems: getDefaultMenuItems,
		
		resetMenu: function () {
			app.layout.menuItems.set('items', getDefaultMenuItems());
        },
		
		menuItems: kendo.observable({items: getDefaultMenuItems()}),
		
		syncSurveys: function (e) {
	        if (typeof app.data != 'undefined' && typeof app.data.surveys == 'object') {
				var items = app.layout.menuItems.get('items').toJSON().map(function(value, index) { return value; });
				var newItems = items.reduce(function (e, i, w) {
					if (w < items.length - 3) {
						e.push(i);
                    }
					
					return e;
                }, []);
				var lastItems = items.reduce(function (e, i, w) {
					if (w >= items.length - 3) {
						e.push(i);
                    }
					
					return e;
                }, [])
				
				var changes = false;
	            
	            //add new surveys to dropdown menu
	            for (var i = 0; i < app.data.surveys.length; i++) {
	                var survey = app.data.surveys[i];
					var pass = false;
					for (var j = 0; j < items.length; j++) {
						var item = items[j];
						if (item.text === survey.definition.name) {
							pass = true;
							break;
                        }
                    }
					if (pass) {
						continue;
                    } else {
						changes = true;
                    }
	                newItems.push({
	                    'type': 'button',
	                    'text': survey.definition.name,
						'url': '#views/survey.html?survey_id='+survey.definition.id,
	                    'overflow': 'always'
	                });
	            }
				for (var n = 0; n < lastItems.length; n++) {
					var lastItem = lastItems[n];
					newItems.push(lastItem);
	            }
				
				if (changes) {
					app.layout.menuItems.set('items', newItems);
                }
	        }
			
        },
		
		createMenu: function (e) {
			e.find('.km-view-title').find('.menu-toolbar').each(function (i, m) {
				if ($(m).getKendoToolBar()) {
					$(m).getKendoToolBar().destroy();
					$(m).remove();
                }
            });
			var menuDiv = $('<div class="menu-toolbar absolute-touch"></div>').appendTo(e.find('.km-view-title'));
			if (!menuDiv.getKendoToolBar()) {
				menuDiv.kendoToolBar({
					transition: 'slide',
	            	items: app.layout.menuItems.get('items')
				});
            }
        },
        
        init: function (e) {},
        
        show: function (e) {},
        
        fillCenter: function (element) {
            var height = element.height();
            var fheight = element.find('.intro-footer').outerHeight(true);
            var fillContent = element.find('.fill-content');
            var centerContent = element.find('.center-content');
            fillContent.height(height - fheight);
            
            var leftover = fillContent.height() - centerContent.height();
            if (leftover > 0) {
                centerContent.css({'margin-top': leftover / 2});
            }
        }
    };
	
	app.layout.resetMenu();
	
	app.layout.menuItems.bind('change', function (e) {
    });
})(window);